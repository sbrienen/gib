package main

import (
	"bytes"
	_ "embed"
	"os/exec"

	"github.com/urfave/cli/v2"
)

//go:embed scripts/update_db.sh
var updateDBScript []byte

func updateDBCmd(c *cli.Context) error {
	cmd := exec.Command("/bin/bash", "-s")
	cmd.Stdin = bytes.NewReader(updateDBScript)
	cmd.Stdout = c.App.Writer
	cmd.Stderr = c.App.Writer
	return cmd.Run()
}
