package main

import (
	"encoding/json"
	"fmt"
	"net/url"
	"os"
	"path"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"github.com/xanzy/go-gitlab"
	"sigs.k8s.io/yaml"
)

// OPAViolations is the output of `opa eval`
type OPAViolations struct {
	Result []struct {
		Expressions []struct {
			Value struct {
				Key string `json:"key"`
				Msg string `json:"msg"`
			} `json:"value"`
			Text     string `json:"text"`
			Location struct {
				Row int `json:"row"`
				Col int `json:"col"`
			} `json:"location"`
		} `json:"expressions"`
		Bindings struct {
			X struct {
				Key string `json:"key"`
				Msg string `json:"msg"`
			} `json:"x"`
		} `json:"bindings"`
	} `json:"result"`
}

// Violation is a Policy violation, exported with the scripts/compliance.sh script
type Violation struct {
	ProjectID         int           `json:"project_id"`
	PathWithNamespace string        `json:"path_with_namespace"`
	Message           string        `json:"message"`
	Key               string        `json:"key"`
	Description       string        `json:"description"`
	State             string        `json:"state,omitempty"`
	Issue             *gitlab.Issue `json:"issue"`
}

const (
	stateNew      = "new"
	stateResolved = "resolved"
	stateReopened = "reopened"
	stateIgnored  = "ignored"

	labelResolved = "resolved"
	labelIgnore   = "ignore"
)

// ViolationLabel return the name of the label for the violation
func (v Violation) ViolationLabel() string {
	return fmt.Sprintf("violation::%s", v.Key)
}

// ProjectLabel return the name of the label for the violation
func (v Violation) ProjectLabel() string {
	return fmt.Sprintf("project_id::%d", v.ProjectID)
}

// Title generates a title for GitLab Issue to be created for this violation
func (v Violation) Title() string {
	return fmt.Sprintf("%s: %s", v.PathWithNamespace, v.Message)
}

// Labels returns the list of labels for a violation
func (v Violation) Labels() gitlab.Labels {
	return gitlab.Labels{v.ViolationLabel(), v.ProjectLabel()}
}

func createIssuesForViolationsCmd(c *cli.Context) error {
	// Load violations
	violations, err := loadViolations(c.String("violations-file"))
	if err != nil {
		return err
	}

	// Fetch project labels
	git := newClient(c)

	pid := c.String("project-id")
	labels, err := fetchLabels(git, pid)
	if err != nil {
		return err
	}

	// Create missing labels
	for label, color := range missingLabels(violations, labels) {
		_, _, err := git.Labels.CreateLabel(pid, &gitlab.CreateLabelOptions{Name: gitlab.String(label), Color: gitlab.String(color)})
		if err != nil {
			return fmt.Errorf("Error while creating label %q: %v", label, err)
		}
	}

	// Fetch issues
	issues, err := fetchIssues(git, pid)
	if err != nil {
		return err
	}

	// Complete violations with missing issues
	u, err := url.Parse(c.String(apiBaseURLFlagName)) // Used to build the project url in the issue description
	if err != nil {
		return err
	}
	savedBaseURLPath := u.Path

	existingCount := 0
	for i := range violations {
		violation := violations[i]
		for _, issue := range issues {
			if issue.Title == violation.Title() { // This is the issue for this violation
				issueLogger := log.WithFields(log.Fields{"project": violation.PathWithNamespace, "title": issue.Title, "URL": issue.WebURL})
				existingCount++
				if hasIgnoreLabel(issue) {
					issueLogger.Info("ignoring violation")
					violation.State = stateIgnored
					violation.Issue = issue
					break
				}
				if issue.State == "closed" {
					issueLogger.Info("reopeing issue")
					issue, _, err = git.Issues.UpdateIssue(pid, issue.IID, &gitlab.UpdateIssueOptions{StateEvent: gitlab.String("reopen")})
					if err != nil {
						return err
					}
					violation.State = stateReopened
				}
				if hasResolvedLabel(issue) {
					issueLogger.Info("Remove 'Resolved' label from issue")
					issue, _, err = git.Issues.UpdateIssue(pid, issue.IID, &gitlab.UpdateIssueOptions{RemoveLabels: &gitlab.Labels{labelResolved}})
					if err != nil {
						return err
					}
					violation.State = stateReopened
				}
				violation.Issue = issue // issue could have been updated by API calls
				break
			}
		}

		u.Path = path.Join(savedBaseURLPath, violation.PathWithNamespace)
		if violation.Issue == nil {
			violation.State = stateNew
			violation.Issue = &gitlab.Issue{
				Title: violation.Title(),
				Description: fmt.Sprintf("Violation detected in project [%s](%s):\r\n\r\n%s",
					violation.PathWithNamespace,
					u,
					violation.Description),
				Labels: violation.Labels(),
			}
		}
	}
	log.Infof("%d issue(s) already created", existingCount)

	// Mark issues not in violations as resolved
	for _, issue := range issues {
		found := false
		for _, violation := range violations {
			if issue.Title == violation.Title() {
				found = true
				break
			}
		}
		if !found {
			// Add a resolved violation to the list
			s := strings.SplitN(issue.Title, ": ", 2)
			if len(s) != 2 {
				// Ignore
				continue
			}
			resolvedViolation := &Violation{
				PathWithNamespace: s[0],
				Message:           s[1],
				Description:       s[1],
				State:             stateResolved,
			}
			violations = append(violations, resolvedViolation)
			log.WithFields(log.Fields{"project": resolvedViolation.PathWithNamespace, "title": issue.Title, "URL": issue.WebURL}).Info("Mark violation issue as resolved")
			resolvedViolation.Issue, _, err = git.Issues.UpdateIssue(pid, issue.IID, &gitlab.UpdateIssueOptions{AddLabels: &gitlab.Labels{labelResolved}})
			if err != nil {
				return err
			}
		}
	}

	// Create missing issues
	for i := range violations {
		violation := violations[i]
		if violation.Issue.ID != 0 {
			continue
		}
		issue := violation.Issue

		opts := &gitlab.CreateIssueOptions{
			Title:       gitlab.String(issue.Title),
			Description: gitlab.String(issue.Description),
			Labels:      &issue.Labels,
		}
		log.WithFields(log.Fields{"project": violation.PathWithNamespace, "violation_key": violation.Key}).Info("Create new issue")
		violation.Issue, _, err = git.Issues.CreateIssue(pid, opts)
		if err != nil {
			return err
		}
	}

	// Store violations with their issues
	vJSON, err := json.Marshal(violations)
	if err != nil {
		return err
	}

	return os.WriteFile(c.String("violations-file"), vJSON, 0600)
}

func newClient(c *cli.Context) *gitlab.Client {
	var git *gitlab.Client
	var err error
	switch {
	case c.String(apiTokenFlagName) != "":
		git, err = gitlab.NewClient(c.String(apiTokenFlagName), gitlab.WithBaseURL(c.String(apiBaseURLFlagName)))
		if err != nil {
			log.Fatalf("Failed to create client: %v", err)
		}
	case c.String("username") != "" && c.String("password") != "":
		git, err = gitlab.NewClient(c.String(apiTokenFlagName), gitlab.WithBaseURL(c.String(apiBaseURLFlagName)))
		if err != nil {
			log.Fatalf("Failed to create client: %v", err)
		}
	default:
		if err := cli.ShowSubcommandHelp(c); err != nil {
			log.Error(err)
		}
		log.Fatalf(`Provide %q or %q + %q to authenticate`, apiTokenFlagName, usernameFlagName, passwordFlagName)
	}
	return git
}

func missingLabels(violations []*Violation, labels []*gitlab.Label) map[string]string {
	missingLabels := make(map[string]string)
	for _, v := range violations {
		if !containsLabel(labels, v.ViolationLabel()) {
			missingLabels[v.ViolationLabel()] = "red"
		}

		if !containsLabel(labels, v.ProjectLabel()) {
			missingLabels[v.ProjectLabel()] = "silver"
		}
	}
	// Resolved label
	if !containsLabel(labels, labelResolved) {
		missingLabels[labelResolved] = "green"
	}
	// Ignore label
	if !containsLabel(labels, labelIgnore) {
		missingLabels[labelIgnore] = "gray"
	}
	return missingLabels
}

func containsLabel(labels []*gitlab.Label, label string) bool {
	for _, l := range labels {
		if l.Name == label {
			return true
		}
	}
	return false
}

func hasIgnoreLabel(issue *gitlab.Issue) bool {
	for _, l := range issue.Labels {
		if l == labelIgnore {
			return true
		}
	}
	return false
}

func hasResolvedLabel(issue *gitlab.Issue) bool {
	for _, l := range issue.Labels {
		if l == labelResolved {
			return true
		}
	}
	return false
}

func loadViolations(path string) ([]*Violation, error) {
	vv := []*Violation{}
	b, err := os.ReadFile(path)
	if err != nil {
		return vv, fmt.Errorf("Error loading violations from file %q: %v", path, err)
	}
	if string(b) == "" {
		return vv, fmt.Errorf("Violations file %q is empty", path)
	}
	err = yaml.Unmarshal(b, &vv)
	return vv, err
}

func fetchLabels(git *gitlab.Client, pid string) ([]*gitlab.Label, error) {
	page := 1
	labels := []*gitlab.Label{}

	for page != 0 {
		opts := &gitlab.ListLabelsOptions{
			IncludeAncestorGroups: gitlab.Bool(false),
			ListOptions: gitlab.ListOptions{
				Page:    page,
				PerPage: 100,
			},
		}
		labelsPage, response, err := git.Labels.ListLabels(pid, opts)
		if err != nil {
			return labels, fmt.Errorf("Error while fetching labels in project %q: %v", pid, err)
		}
		page = response.NextPage
		labels = append(labels, labelsPage...)
	}
	return labels, nil
}

func fetchIssues(git *gitlab.Client, pid string) ([]*gitlab.Issue, error) {
	page := 1
	issues := []*gitlab.Issue{}

	for page != 0 {
		opts := &gitlab.ListProjectIssuesOptions{
			ListOptions: gitlab.ListOptions{
				Page:    page,
				PerPage: 100,
			},
		}
		issuesPage, response, err := git.Issues.ListProjectIssues(pid, opts)
		if err != nil {
			return issues, fmt.Errorf("Error while fetching issues in project %q: %v", pid, err)
		}
		page = response.NextPage
		issues = append(issues, issuesPage...)
	}
	return issues, nil
}
