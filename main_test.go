package main

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/mux"
)

func testServer(t *testing.T) (*mux.Router, *httptest.Server) {
	mux := mux.NewRouter().UseEncodedPath()

	// Add a middleware to check header on all requests
	mux.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// by default, return only 1 page per query
			w.Header().Set("x-total-pages", "1")

			// The REST and GraphQL APIs use different authentication headers
			switch {
			case strings.HasPrefix(r.URL.Path, "/api/v4/"): // REST API
				if auth := r.Header.Get("Private-Token"); auth != "asdf123" {
					t.Errorf("[%s] Authorization header: expected: asdf123, got: %s\n", r.URL, auth)
				}
			case strings.HasPrefix(r.URL.Path, "/api/graphql"): // GraphQL API
				if auth := r.Header.Get("Authorization"); auth != "Bearer asdf123" {
					t.Errorf("[%s] Authorization header: expected: Bearer asdf123, got: %s\n", r.URL, auth)
				}
			default: // Other URLs than the APIs should be an error
				t.Errorf("Unknown path: %s", r.URL.Path)
			}

			if r.Method == http.MethodPost {
				if ct := r.Header.Get("Content-Type"); ct != "application/json" {
					t.Errorf("Content-Type header: expected: application/json, got: %s\n", ct)
				}
			}
			// Call the next handler, which can be another middleware in the chain, or the final handler.
			next.ServeHTTP(w, r)
		})
	})

	return mux, httptest.NewServer(mux)
}
