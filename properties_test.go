package main

import (
	"testing"

	"sigs.k8s.io/yaml"
)

func TestProperties(t *testing.T) {
	t.Run("Categories", func(t *testing.T) {
		b := []byte(
			`categories:
  - product
  - api
  - internal
  - red_data
`)

		var p Properties

		err := yaml.Unmarshal(b, &p)
		if err != nil {
			t.Error("Expected err to be nil, got: ", err)
		}
	})

	t.Run("URLs", func(t *testing.T) {
		b := []byte(
			`urls:
  - https://gitlab.com
  - https://gitlab.com/api/v4
`)

		var p Properties

		err := yaml.Unmarshal(b, &p)
		if err != nil {
			t.Error("Expected err to be nil, got: ", err)
		}
	})
}
