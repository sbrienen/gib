# Vulnerabilities must be triaged under 180 days
# See https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/91396


package gitlab.vulnerabilities

detected_vulnerabilites[vulnerability] {
  vulnerability := input[_]
  vulnerability.state == "detected"
}

over_slo[vulnerability] {
  vulnerability := detected_vulnerabilites[_]
  ns := time.parse_rfc3339_ns(vulnerability.created_at)
  ns <= time.add_date(time.now_ns(),0,0,-180)
}

violation[{"msg": msg, "description": description, "key": "vulnerabilities_over_slo"}]  {
  count(over_slo) > 0
  msg := "Vulnerabilities over SLO (180 days)"
  description := sprintf("%d vulnerabilities over SLO (180 days)", [count(over_slo)])
}
