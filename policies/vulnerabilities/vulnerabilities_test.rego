package gitlab.vulnerabilities

test_project_with_vulns_over_180_days {
  vulnerabilities := [{
    "state": "detected",
    "created_at": "2000-11-30T18:41:16.617Z",
  },
  {
    "state": "resolved",
    "created_at": "2000-11-30T18:41:16.617Z",
  }]

  result := violation[x] with input as vulnerabilities
  result == {"msg": "Vulnerabilities over SLO (180 days)", "description": "1 vulnerabilities over SLO (180 days)", "key": "vulnerabilities_over_slo"}
}

test_project_without_vulns_over_180_days {
  vulnerabilities := [{
    "state": "confirmed",
    "created_at": "2000-11-30T18:41:16.617Z",
  },
  {
    "state": "resolved",
    "created_at": "2000-11-30T18:41:16.617Z",
  }]

  result := violation with input as vulnerabilities
  count(result) == 0
}

