package gitlab.sites

grade = g {
  input[i].id == "overall_grade"
  g := input[i].finding
}

# default grade_at_least_A = false

grade_at_least_A {
  {"A+", "A", "A-"}[_] = grade
}

violation[{"msg": "Grade should be at least \"A\"", "description": msg, "key": "grade_not_A"}] {
  not grade_at_least_A
  msg := sprintf("Grade should be at least \"A\", got %q", [grade])
}
