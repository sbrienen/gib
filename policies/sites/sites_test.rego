package gitlab.sites

test_site_with_b_grade {
  input := [{
    "id": "overall_grade",
    "ip": "customers.gitlab.com/104.18.27.123",
    "port": "443",
    "severity": "OK",
    "finding": "B",
  }]

  violation[x] == {"msg": "Grade should be at least \"A\"", "description": "Grade should be at least \"A\", got \"B\"", "key": "grade_not_A"} with input as input
}

test_site_with_a_grade {
  input := [{
    "id": "overall_grade",
    "ip": "customers.gitlab.com/104.18.27.123",
    "port": "443",
    "severity": "OK",
    "finding": "A+",
  }]

  count(violation) == 0 with input as input
}
