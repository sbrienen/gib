package gitlab.projects.protected_branches

import input.categories
import input.project
import input.protected_branches

default_branch_is_protected {
  default_branch := project.default_branch
  glob.match(protected_branches[_].name, ["*"], default_branch)
}

violation[{"msg": msg, "description": description, "key": key}] {
  categories[_] == "product"
  not default_branch_is_protected
  msg = "Default branch not protected"
  description = "Default branch not protected"
  key = "default_branch_not_protected"
}{
  protected_branches[i].name == project.default_branch
  protected_branches[i].push_access_levels[_].access_level == 30 # Developers + Maintainers
  msg = "Developers can push"
  description = "Developers can push to default branch"
  key = "developers_can_push"
}{
  protected_branches[i].name == project.default_branch
  protected_branches[i].merge_access_levels[_].access_level == 30 # Developers + Maintainers
  msg = "Developers can merge"
  description = "Developers can merge to default branch"
  key = "developers_can_merge"
}
