package gitlab.projects
import data.gitlab.projects.security_jobs
import data.gitlab.projects.visibility
import data.gitlab.projects.tidy
import data.gitlab.projects.protected_branches

violation[{"msg": msg, "description": description, "key": key}] {
  security_jobs.violation[{"msg": msg, "description": description, "key": key}]
}{
  visibility.violation[{"msg": msg, "description": description, "key": key}]
}{
  tidy.violation[{"msg": msg, "description": description, "key": key}]
}{
  protected_branches.violation[{"msg": msg, "description": description, "key": key}]
}
