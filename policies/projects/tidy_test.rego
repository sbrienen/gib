package gitlab.projects.tidy

test_marked_for_deletion {
  violation == {{"msg": "Must be deleted", "description": "Project marked for deletion", "key": "marked_for_deletion"}}
    with input as {"categories": ["marked_for_deletion"]}
}

