package gitlab.projects.security_jobs

test_product_with_no_security_jobs {
  results := violation with input as {"categories": ["product"], "ci-config": {}}
  results == result_no_jobs_configured
}

test_red_data_with_no_security_jobs {
  results := violation with input as {"categories": ["red_data"], "ci-config": {}}
  results == result_no_jobs_configured
}

test_library_with_no_security_jobs {
  results := violation with input as {"categories": ["library"], "ci-config": {}}
  results == result_no_jobs_configured
}

test_product_and_container_with_no_security_jobs {
  results := violation with input as {"categories": ["product", "container"], "ci-config": {}}
  results == {
    {"description": "SAST is not configured", "key": "sast_not_configured", "msg": "SAST is not configured"},
    {"description": "Dependency Scanning is not configured", "key": "dependency_scanning_not_configured", "msg": "Dependency Scanning is not configured"},
    {"description": "Secret Detection is not configured", "key": "secret_detection_not_configured", "msg": "Secret Detection is not configured"},
    {"description": "Container Scanning is not configured", "key": "container_scanning_not_configured", "msg": "Container Scanning is not configured"}
  }
}

test_other_category_with_no_security_jobs {
  results := violation with input as {"categories": ["marked_for_deletion"], "ci-config": {}}
  results == set()
}

test_product_with_all_jobs_configured {
  input := all_jobs_configured("product")
  results := violation with input as input
  # No violations
  results == set()
}

test_product_and_container_with_all_jobs_configured {
  input := {
    "categories": ["product", "container"],
    "ci-config": {
      "bandit-sast": {
        "artifacts": {
          "reports": {
            "sast": [
              "gl-sast-report.json"
            ]
          }
        }
      },
      "gemnasium-python": {
        "artifacts": {
          "reports": {
            "dependency_scanning": [
              "gl-dependency-scanning-report.json"
            ]
          }
        }
      },
      "secret-dectection": {
        "artifacts": {
          "reports": {
            "secret_detection": [
              "gl-secret-detection-report.json"
            ]
          }
        }
      },
      "container-scanning": {
        "artifacts": {
          "reports": {
            "container_scanning": [
              "gl-container-scanning-report.json"
            ]
          }
        }
      }
    }
  }
  results := violation with input as input
  # No violations
  results == set()
}

# Container Scanning now generates two reports, but still want Dependency Scanning as well
test_product_with_all_jobs_configured_except_dependency_scanning {
  input := {
    "categories": ["product"],
    "ci-config": {
      "bandit-sast": {
        "artifacts": {
          "reports": {
            "sast": [
              "gl-sast-report.json"
            ]
          }
        }
      },
      "secret-dectection": {
        "artifacts": {
          "reports": {
            "secret_detection": [
              "gl-secret-detection-report.json"
            ]
          }
        }
      },
      "container-scanning": {
        "artifacts": {
          "reports": {
            "container_scanning": [
              "gl-container-scanning-report.json"
            ],
            "dependency_scanning": [
              "gl-dependency-scanning-report.json"
            ]
          }
        }
      }
    }
  }
  results := violation with input as input
  # No violations
  results == {
    {"description": "Dependency Scanning is not configured", "key": "dependency_scanning_not_configured", "msg": "Dependency Scanning is not configured"}
  }
}

test_red_data_with_all_jobs_configured {
  input := all_jobs_configured("red_data")
  results := violation with input as input
  # No violations
  results == set()
}
test_library_with_all_jobs_configured {
  input := all_jobs_configured("library")
  results := violation with input as input
  # No violations
  results == set()
}

test_product_with_jobs_disabled {
  input := {
    "categories": ["product", "container"],
    "ci-config": {
      "variables": {
        "SAST_DISABLED": "true",
        "DEPENDENCY_SCANNING_DISABLED": "true",
        "SECRET_DETECTION_DISABLED": "true",
        "CONTAINER_SCANNING_DISABLED": "true"
      },
      "bandit-sast": {
        "artifacts": {
          "reports": {
            "sast": [
              "gl-sast-report.json"
            ]
          }
        }
      },
      "gemnasium-python": {
        "artifacts": {
          "reports": {
            "dependency_scanning": [
              "gl-dependency-scanning-report.json"
            ]
          }
        }
      },
      "secret-dectection": {
        "artifacts": {
          "reports": {
            "secret_detection": [
              "gl-secret-detection-report.json"
            ]
          }
        }
      },
      "container-scanning": {
        "artifacts": {
          "reports": {
            "container_scanning": [
              "gl-container-scanning-report.json"
            ]
          }
        }
      }
    }
  }
  results := violation with input as input
  results == {
    {"description": "DEPENDENCY_SCANNING_DISABLED is set to \"true\"", "key": "dependency_scanning_is_disabled", "msg": "DEPENDENCY_SCANNING_DISABLED is not 'false'"},
    {"description": "SAST_DISABLED is set to \"true\"", "key": "sast_is_disabled", "msg": "SAST_DISABLED is not 'false'"},
    {"description": "SECRET_DETECTION_DISABLED is set to \"true\"", "key": "secret_detection_is_disabled", "msg": "SECRET_DETECTION_DISABLED is not 'false'"},
    {"description": "CONTAINER_SCANNING_DISABLED is set to \"true\"", "key": "container_scanning_is_disabled", "msg": "CONTAINER_SCANNING_DISABLED is not 'false'"}
  }
}

#
# `use_pat`, `website`+`external` | [Dependency Scanning] and [Secret Detection] must be enabled |
#
test_use_pat_with_no_security_jobs {
  results := violation with input as {"categories": ["use_pat"], "ci-config": {}}
  results == result_ds_sd_not_configured
}

test_use_pat_with_all_jobs_configures {
  input := all_jobs_configured("use_pat")
  results := violation with input as input
  results == set()
}

test_website_external_with_no_security_jobs {
  results := violation with input as {"categories": ["website", "external"], "ci-config": {}}
  results == result_ds_sd_not_configured
}

test_use_website_external_with_all_jobs_configures {
  input := {
    "categories": ["website", "external"],
    "ci-config": {
      "bandit-sast": {
        "artifacts": {
          "reports": {
            "sast": [
              "gl-sast-report.json"
            ]
          }
        }
      },
      "gemnasium-python": {
        "artifacts": {
          "reports": {
            "dependency_scanning": [
              "gl-dependency-scanning-report.json"
            ]
          }
        }
      },
      "secret-dectection": {
        "artifacts": {
          "reports": {
            "secret_detection": [
              "gl-secret-detection-report.json"
            ]
          }
        }
      }
    }
  }

  results := violation with input as input
  results == set()
}

test_secret_detection_historic_scan_is_set {
  input := {
    "ci-config": {
      "variables": {
        "SECRET_DETECTION_HISTORIC_SCAN": "true"
      }
    }
  }

  results := violation with input as input
  results == {
    {"description": "SECRET_DETECTION_HISTORIC_SCAN is set to \"true\"", "key": "secrets_historic_scan_is_enabled", "msg": "SECRET_DETECTION_HISTORIC_SCAN is not 'false'"},
  }
}

test_secret_detection_historic_scan_is_set_in_job {
  input := {
    "ci-config": {
      "secret-dectection": {
        "variables": {
          "SECRET_DETECTION_HISTORIC_SCAN": "true",
        },
        "artifacts": {
          "reports": {
            "secret_detection": [
              "gl-secret-detection-report.json"
            ]
          }
        }
      }
    }
  }

  results := violation with input as input
  results == {
    {"description": "SECRET_DETECTION_HISTORIC_SCAN is set to \"true\"", "key": "secrets_historic_scan_is_enabled", "msg": "SECRET_DETECTION_HISTORIC_SCAN is not 'false'"},
  }
}

all_jobs_configured(category) = output {
output = {
    "categories": [category],
    "ci-config": {
      "bandit-sast": {
        "artifacts": {
          "reports": {
            "sast": [
              "gl-sast-report.json"
            ]
          }
        }
      },
      "gemnasium-python": {
        "artifacts": {
          "reports": {
            "dependency_scanning": [
              "gl-dependency-scanning-report.json"
            ]
          }
        }
      },
      "secret-dectection": {
        "artifacts": {
          "reports": {
            "secret_detection": [
              "gl-secret-detection-report.json"
            ]
          }
        }
      }
    }
  }
}

result_no_jobs_configured = {
    {"description": "SAST is not configured", "key": "sast_not_configured", "msg": "SAST is not configured"},
    {"description": "Dependency Scanning is not configured", "key": "dependency_scanning_not_configured", "msg": "Dependency Scanning is not configured"},
    {"description": "Secret Detection is not configured", "key": "secret_detection_not_configured", "msg": "Secret Detection is not configured"}
}

result_ds_sd_not_configured = {
    {"description": "Dependency Scanning is not configured", "key": "dependency_scanning_not_configured", "msg": "Dependency Scanning is not configured"},
    {"description": "Secret Detection is not configured", "key": "secret_detection_not_configured", "msg": "Secret Detection is not configured"}
}
