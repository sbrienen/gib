package gitlab.projects.security_jobs

import input.categories
import input["ci-config"] as ci

reports[name] { x := ci[_].artifacts.reports[name] }

sast_configured { reports["sast"] }
# Container Scanning defines 2 reports: one for Dependency Scanning, one for Container Scanning
# If we only test the presence of the Dependency Scanning one, we could actually miss Dependency Scanning
dependency_scanning_configured {
  some i
  ci[i].artifacts.reports["dependency_scanning"]
  not ci[i].artifacts.reports["container_scanning"]
}
container_scanning_configured { reports["container_scanning"] }
dast_configured { reports["dast"] }
secret_detection_configured { reports["secret_detection"] }

sast[{"msg": msg, "description": description, "key": key}] {
  key = "sast_not_configured"
  not sast_configured
  msg := "SAST is not configured"
  description := "SAST is not configured"
}{
  key = "sast_is_disabled"
  v := ci.variables["SAST_DISABLED"]
  v != "false"
  msg := "SAST_DISABLED is not 'false'"
  description := sprintf("SAST_DISABLED is set to %q", [v])
}

dependency_scanning[{"msg": msg, "description": description, "key": key}] {
  key = "dependency_scanning_not_configured"
  not dependency_scanning_configured
  msg := "Dependency Scanning is not configured"
  description := "Dependency Scanning is not configured"
}{
  key = "dependency_scanning_is_disabled"
  v := ci.variables["DEPENDENCY_SCANNING_DISABLED"]
  v != "false"
  msg := "DEPENDENCY_SCANNING_DISABLED is not 'false'"
  description := sprintf("DEPENDENCY_SCANNING_DISABLED is set to %q", [v])
}

secret_detection[{"msg": msg, "description": description, "key": key}] {
  key = "secret_detection_not_configured"
  not secret_detection_configured
  msg := "Secret Detection is not configured"
  description := "Secret Detection is not configured"
}{
  key = "secret_detection_is_disabled"
  v := ci.variables["SECRET_DETECTION_DISABLED"]
  v != "false"
  msg := "SECRET_DETECTION_DISABLED is not 'false'"
  description := sprintf("SECRET_DETECTION_DISABLED is set to %q", [v])
}

container_scanning[{"msg": msg, "description": description, "key": key}] {
  key = "container_scanning_not_configured"
  not container_scanning_configured
  msg := "Container Scanning is not configured"
  description := "Container Scanning is not configured"
}{
  key = "container_scanning_is_disabled"
  v := ci.variables["CONTAINER_SCANNING_DISABLED"]
  v != "false"
  msg := "CONTAINER_SCANNING_DISABLED is not 'false'"
  description := sprintf("CONTAINER_SCANNING_DISABLED is set to %q", [v])
}

# Don't scan the whole repo history every time
secret_detection_historic_scan[{"msg": msg, "description": description, "key": key}] {
  v:= secrets_historic_scan_is_enabled
  v != "false"
  key = "secrets_historic_scan_is_enabled"
  msg := "SECRET_DETECTION_HISTORIC_SCAN is not 'false'"
  description := sprintf("SECRET_DETECTION_HISTORIC_SCAN is set to %q", [v])
}

secrets_historic_scan_is_enabled {
  ci.variables["SECRET_DETECTION_HISTORIC_SCAN"]
}{
  ci[_].variables["SECRET_DETECTION_HISTORIC_SCAN"]
}

# `red-data`, `product`, `library` | [SAST], [Dependency Scanning] and [Secret Detection] must be enabled |
violation[{"msg": msg, "description": description, "key": key}] {
  any([categories[_] == "product",
       categories[_] == "red_data",
       categories[_] == "library"])
  sast[{"msg": msg, "description": description, "key": key}]
}{
  any([categories[_] == "product",
       categories[_] == "red_data",
       categories[_] == "library"])
  dependency_scanning[{"msg": msg, "description": description, "key": key}]
}{
  any([categories[_] == "product",
       categories[_] == "red_data",
       categories[_] == "library"])
  secret_detection[{"msg": msg, "description": description, "key": key}]
}
# `product` + `container` | [Container Scanning] must be enabled |
{
  categories[_] == "product"
  categories[_] == "container"
  container_scanning[{"msg": msg, "description": description, "key": key}]
}
# `use_pat`, `website`+`external` | [Dependency Scanning] and [Secret Detection] must be enabled |
{
  categories[_] == "use_pat"
  dependency_scanning[{"msg": msg, "description": description, "key": key}]
}{
  categories[_] == "use_pat"
  secret_detection[{"msg": msg, "description": description, "key": key}]
}{
  categories[_] == "website"
  categories[_] == "external"
  dependency_scanning[{"msg": msg, "description": description, "key": key}]
}{
  categories[_] == "website"
  categories[_] == "external"
  secret_detection[{"msg": msg, "description": description, "key": key}]
}{
  secret_detection_historic_scan[{"msg": msg, "description": description, "key": key}]
}

