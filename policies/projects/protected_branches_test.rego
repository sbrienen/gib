package gitlab.projects.protected_branches

test_default_branch_is_protected {
  input := {
    "categories": ["product"],
    "project": {
      "default_branch": "main",
    },
    "protected_branches": [
      {
        "id":39065558,"name":"main",
        "push_access_levels": [{"access_level":40,"access_level_description":"Maintainers","user_id":null,"group_id":null}],
        "merge_access_levels":[{"access_level":40,"access_level_description":"Maintainers","user_id":null,"group_id":null}],
        "allow_force_push":false,
        "unprotect_access_levels":[],
        "code_owner_approval_required":true
      }
    ]
  }
  results := violation with input as input
  results == set()
}

# test examples from https://docs.gitlab.com/ee/user/project/protected_branches.html
test_default_branch_is_protected_by_wildcard {
  input := {
    "project": {"default_branch": "production-stable"},
    "protected_branches": [{"name":"*-stable"}]
  }
  default_branch_is_protected with input as input
}

test_default_branch_is_protected_by_wildcard_with_slash {
  input := {
    "project": {"default_branch": "production/app-server"},
    "protected_branches": [{"name":"production/*"}]
  }
  default_branch_is_protected with input as input
}

test_default_branch_is_protected_by_wildcard_with_double_wildcard {
  input := {
    "project": {"default_branch": "master/gitlab/production"},
    "protected_branches": [{"name":"*gitlab*"}]
  }
  default_branch_is_protected with input as input
}

test_default_branch_with_dots_is_protected_by_wildcard {
  input := {
    "project": {"default_branch": "v6-14.1-security-checks"},
    "protected_branches": [{"name":"*-security-checks"}]
  }
  default_branch_is_protected with input as input
}

test_default_branch_is_not_protected {
  input := {
    "categories": ["product"],
    "project": {
      "default_branch": "main",
    },
    "protected_branches": []
  }

  results := violation with input as input
  results == {
    {"msg": "Default branch not protected", "description": "Default branch not protected", "key": "default_branch_not_protected"}
  }
}

test_default_branch_allows_developers_to_push {
  input := {
    "categories": ["product"],
    "project": {
      "default_branch": "main",
    },
    "protected_branches": [
      {
        "id":39065558,"name":"main",
        "push_access_levels": [
          {"access_level":40,"access_level_description":"Maintainers","user_id":null,"group_id":null},
          {"access_level":30,"access_level_description":"Developers + Maintainers","user_id":null,"group_id":null}
          ],
        "merge_access_levels":[{"access_level":40,"access_level_description":"Maintainers","user_id":null,"group_id":null}],
        "allow_force_push":false,
        "unprotect_access_levels":[],
        "code_owner_approval_required":true
      }
    ]
  }
  results := violation with input as input
  results == {
    {"msg": "Developers can push", "description": "Developers can push to default branch", "key": "developers_can_push"}
  }
}

test_default_branch_allows_developers_to_merge {
  input := {
    "categories": ["product"],
    "project": {
      "default_branch": "main",
    },
    "protected_branches": [
      {
        "id":39065558,"name":"main",
        "push_access_levels": [{"access_level":40,"access_level_description":"Maintainers","user_id":null,"group_id":null},],
        "merge_access_levels": [
          {"access_level":40,"access_level_description":"Maintainers","user_id":null,"group_id":null},
          {"access_level":30,"access_level_description":"Developers + Maintainers","user_id":null,"group_id":null}
          ],
        "allow_force_push":false,
        "unprotect_access_levels":[],
        "code_owner_approval_required":true
      }
    ]
  }
  results := violation with input as input
  results == {
    {"msg": "Developers can merge", "description": "Developers can merge to default branch", "key": "developers_can_merge"}
  }
}

test_another_branch_allows_developers_to_push {
  input := {
    "categories": ["product"],
    "project": {
      "default_branch": "main",
    },
    "protected_branches": [
      {
        "id":39065558,"name":"main",
        "push_access_levels": [{"access_level":40,"access_level_description":"Maintainers","user_id":null,"group_id":null},],
        "merge_access_levels":[{"access_level":40,"access_level_description":"Maintainers","user_id":null,"group_id":null}],
        "allow_force_push":false,
        "unprotect_access_levels":[],
        "code_owner_approval_required":true
      },
      {
        "id":39065558,"name":"another_branch",
        "push_access_levels": [
          {"access_level":40,"access_level_description":"Maintainers","user_id":null,"group_id":null},
          {"access_level":30,"access_level_description":"Developers + Maintainers","user_id":null,"group_id":null}
          ],
        "merge_access_levels":[{"access_level":40,"access_level_description":"Maintainers","user_id":null,"group_id":null}],
        "allow_force_push":false,
        "unprotect_access_levels":[],
        "code_owner_approval_required":true
      }
    ]
  }
  results := violation with input as input
  results == set()
}
