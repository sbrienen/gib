package gitlab.projects.visibility

import input.project
import input.categories

violation[{"msg": "Project can't be public", "description": "Private project must remain private", "key": "private_project_exposed"}] {
  categories[_] == "keep_private"
  project.visibility == "public"
}

violation[{"msg": "Internal visibility", "description": "Internal visibility is forbidden", "key": "internal_visibility"}] {
  project.visibility == "internal"
}
