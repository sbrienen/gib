package main

import (
	"encoding/json"
	"time"

	log "github.com/sirupsen/logrus"
)

// Vulnerability is a project vulnerability
// See https://docs.gitlab.com/ee/api/project_vulnerabilities.html
// Note: Several (useless for us) field are omitted on purpose
type Vulnerability struct {
	// AuthorID int `json:"author_id"`
	// Confidence    string     `json:"confidence"`
	CreatedAt time.Time `json:"created_at"`
	// Description   string     `json:"description"`
	DismissedAt   *time.Time `json:"dismissed_at"`
	DismissedByID *int       `json:"dismissed_by_id"`
	// DueDate       time.Time  `json:"due_date"`
	Finding struct {
		//Confidence          string    `json:"confidence"`
		//CreatedAt           time.Time `json:"created_at"`
		//ID                  int       `json:"id"`
		//LocationFingerprint string    `json:"location_fingerprint"`
		//MetadataVersion     string    `json:"metadata_version"`
		Description string `json:"description"`
		//Message							string `json:"message"`
		//Name                string    `json:"name"`
		//PrimaryIdentifierID int       `json:"primary_identifier_id"`
		//ProjectFingerprint  string    `json:"project_fingerprint"`
		//ProjectID           int       `json:"project_id"`
		RawMetadata string `json:"raw_metadata"`
		//ReportType          string    `json:"report_type"`
		//ScannerID           int       `json:"scanner_id"`
		//Severity            string    `json:"severity"`
		//UpdatedAt           time.Time `json:"updated_at"`
		//UUID                string    `json:"uuid"`
		//VulnerabilityID     int       `json:"vulnerability_id"`
	} `json:"finding"`
	ID int `json:"id"`
	//LastEditedAt   *time.Time `json:"last_edited_at"`
	//LastEditedByID *int       `json:"last_edited_by_id"`
	Project struct {
		//CreatedAt         time.Time `json:"created_at"`
		//Description       string    `json:"description"`
		ID int `json:"id"`
		//Name              string    `json:"name"`
		//NameWithNameSpace string    `json:"name_with_name_space"`
		//Path              string    `json:"path"`
		//PathWithNamespace string    `json:"path_with_namespace"`
	} `json:"project"`
	//ProjectDefaultBranch    string     `json:"project_default_branch"`
	ProjectID               int        `json:"project_id"`
	ReportType              string     `json:"report_type"`
	ResolvedAt              *time.Time `json:"resolved_at"`
	ResolvedBy              *int       `json:"resolved_by"`
	ResolvedOnDefaultBranch bool       `json:"resolved_on_default_branch"`
	Severity                string     `json:"severity"`
	//StartDate               *time.Time `json:"start_date"`
	State     string     `json:"state"`
	Title     string     `json:"title"`
	UpdatedAt *time.Time `json:"updated_at"`
	// UpdatedByID *int       `json:"updated_by_id"`
}

// MarshalJSON filters out more fields (`finding`) and deserialize `finding.raw_metadata` to flatten it in the output
func (v Vulnerability) MarshalJSON() ([]byte, error) {
	type vulnerability Vulnerability
	x := vulnerability(v)

	type rawMetaData struct {
		Description string `json:"description,omitempty"`
		Scanner     struct {
			ID   string `json:"id"`
			Name string `json:"name"`
		} `json:"scanner"`
		Location struct {
			File   string `json:"file"`
			Commit struct {
				Date time.Time `json:"date"`
				Sha  string    `json:"sha"`
			} `json:"commit"`
			StartLine int `json:"start_line,omitempty"`
			EndLine   int `json:"end_line,omitempty"`
		} `json:"location"`
		Identifiers []struct {
			Type  string `json:"type"`
			Name  string `json:"name"`
			Value string `json:"value"`
		} `json:"identifiers"`
		Remediations []interface{} `json:"remediations"`
	}
	raw := rawMetaData{}
	err := json.Unmarshal([]byte(v.Finding.RawMetadata), &raw)
	if err != nil {
		log.WithFields(log.Fields{"id": v.ID, "project_id": v.Project.ID}).Errorf("Invalid rawMetaData: %s", v.Finding.RawMetadata)
	}

	exportedVuln := struct {
		*vulnerability
		// Finding is used to Unmarshal values, but we omit it when Marshaling
		Finding     omit   `json:"finding,omitempty"`
		Project     omit   `json:"project,omitempty"`
		ProjectID   int    `json:"project_id"`
		Description string `json:"description"`
		*rawMetaData
	}{
		vulnerability: &x,
		ProjectID:     x.Project.ID,
		Description:   x.Finding.Description,
		rawMetaData:   &raw,
	}

	return json.Marshal(exportedVuln)
}
