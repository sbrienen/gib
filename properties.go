package main

import (
	"encoding/json"
	"fmt"
	"net/url"
)

// Properties holds the properties of projects
type Properties struct {
	Categories   []Category   `json:"categories"`
	URLs         []JSONURL    `json:"urls"`
	Dependencies []Dependency `json:"dependencies"`
}

// Category is the name of a project category
type Category string

// Categories are currently hardcoded
// TODO: Make them configurable via a config file
const (
	// CategoryProduct is a project part of the GitLab Product
	CategoryProduct = "product"

	// CategoryLibrary refers to libraries (Ruby Gem for example)
	CategoryLibrary = "library"
	// CategoryWebsite refers to Website (use with `internal`/`external`)
	CategoryWebsite = "website"
	// CategoryAPI refers to APIs (use with `internal`/`external`)
	CategoryAPI = "api"
	// CategoryService refers to projects exposing ports
	CategoryService = "service"
	// CategoryDeploy refers to projects used to deploy GitLab (like Helm Charts)
	CategoryDeploy = "deploy"

	// CategoryInternal refers to user-facing applications, internal only
	CategoryInternal = "internal"
	// CategoryExternal refers to user-facing applications, open to the WWW
	CategoryExternal = "external"

	// Data classification
	// https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html

	// CategoryGreenData refers to Green Data
	CategoryGreenData = "green_data"
	// CategoryYellowData refers to Yellow Data
	CategoryYellowData = "yellow_data"
	// CategoryOrangeData refers to Orange Data
	CategoryOrangeData = "orange_data"
	// CategoryRedData refers to Red Data
	CategoryRedData = "red_data"

	// Category3rdParty refers to projects integrating with 3rd parties
	Category3rdParty = "3rd_party"

	// POCs, tests, and demos, nothing to see here.

	// CategoryDemo is for demos
	CategoryDemo = "demo"
	// CategoryTest is for test projects
	CategoryTest = "test"
	// CategoryPOC is for POCs
	CategoryPOC = "poc"

	// CategoryTeam is used for team work (tasks, organization, ...). No code
	CategoryTeam = "team"

	// CategoryTemporary is for temporary projects, should be `marked_for_deletion` after a certain time
	CategoryTemporary = "temporary"

	// CategoryUsePAT is for projects using Personal (or Project) Access Token
	CategoryUsePAT = "use_pat"

	// CategoryDeprecated is for deprecated projects (but should keep)
	CategoryDeprecated = "deprecated"

	// CategoryMarkedForDeletion is for projects we should remove altogether
	CategoryMarkedForDeletion = "marked_for_deletion"

	// CategoryKeepPrivate is for projects we should keep private indefinitely
	CategoryKeepPrivate = "keep_private"

	// CategoryDocs is for projects generating documentation
	CategoryDocs = "docs"

	// CategoryTooling is for tooling projects
	CategoryTooling = "tooling"

	// CategoryContainers is for projects building Docker images
	CategoryContainer = "container"
)

// PropertiesFile is the name of the projects properties file
const PropertiesFile = "properties.yml"

// JSONURL is used to marshal/unmarshal URLs
type JSONURL struct {
	*url.URL
}

// UnmarshalJSON parses the URL represented as a string
func (j *JSONURL) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	url, err := url.Parse(s)
	if err != nil {
		return err
	}

	if url.Host == "" {
		return fmt.Errorf("invalid url, hostname is empty")
	}
	j.URL = url
	return nil
}

// MarshalJSON marshals the string representation of the underlying URL
func (j JSONURL) MarshalJSON() ([]byte, error) {
	var s string
	if j.URL != nil {
		s = j.URL.String()
	}
	return json.Marshal(s)
}

// ParseCategory parse a string category and returns the corresponding Category, or "".
func ParseCategory(cat string) Category {
	switch cat {
	case "product":
		return CategoryProduct

	case "library":
		return CategoryLibrary
	case "website":
		return CategoryWebsite
	case "api":
		return CategoryAPI
	case "service":
		return CategoryService
	case "deploy":
		return CategoryDeploy

	case "internal":
		return CategoryInternal
	case "external":
		return CategoryExternal

	case "green_data":
		return CategoryGreenData
	case "yellow_data":
		return CategoryYellowData
	case "orange_data":
		return CategoryOrangeData
	case "red_data":
		return CategoryRedData

	case "3rd_party":
		return Category3rdParty

	case "demo":
		return CategoryDemo
	case "test":
		return CategoryTest
	case "poc":
		return CategoryPOC

	case "team":
		return CategoryTeam

	case "temporary":
		return CategoryTemporary

	case "deprecated":
		return CategoryDeprecated

	case "use_pat":
		return CategoryUsePAT

	case "marked_for_deletion":
		return CategoryMarkedForDeletion

	case "keep_private":
		return CategoryKeepPrivate

	case "docs":
		return CategoryDocs

	case "tooling":
		return CategoryTooling

	case "container":
		return CategoryContainer
	}
	return ""
}

// UnmarshalJSON parses the category as a string
func (c *Category) UnmarshalJSON(b []byte) error {
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	*c = ParseCategory(s)
	if *c == Category("") {
		return fmt.Errorf("unknown category: %q", s)
	}
	return nil
}
