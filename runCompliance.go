package main

import (
	"bytes"
	_ "embed"
	"os/exec"

	"github.com/urfave/cli/v2"
)

//go:embed scripts/compliance.sh
var complianceScript []byte

func runCompliance(c *cli.Context) error {
	args := []string{}
	if c.NArg() > 0 {
		args = append([]string{"-s"}, c.Args().Slice()...)
	}
	cmd := exec.Command("/bin/bash", args...)
	cmd.Stdin = bytes.NewReader(complianceScript)
	cmd.Stdout = c.App.Writer
	cmd.Stderr = c.App.Writer
	return cmd.Run()
}
