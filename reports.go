package main

import (
	"bytes"
	_ "embed"
	"os/exec"

	"github.com/urfave/cli/v2"
)

//go:embed scripts/etl.sh
var etlScript []byte

func generateReportsCmd(c *cli.Context) error {
	cmd := exec.Command("/bin/bash", "-s")
	cmd.Stdin = bytes.NewReader(etlScript)
	cmd.Stdout = c.App.Writer
	cmd.Stderr = c.App.Writer
	return cmd.Run()
}
