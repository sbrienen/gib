package main

import (
	"encoding/json"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"regexp"

	log "github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
	"sigs.k8s.io/yaml"

	"github.com/urfave/cli/v2"
)

func validateCmd(c *cli.Context) error {
	dir := c.Args().First()
	dataDir := c.String(dataDirFlagName)
	var numDirs int

	if dir == "" {
		dir = dataDir
	}
	err := filepath.WalkDir(dir, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		// Validate regular files
		if !d.IsDir() {
			err := validateFile(path, d, c.Bool("quick"))
			if err != nil {
				log.WithField("path", path).Error("Invalid file")
				return err
			}
			return nil
		}

		if path != dataDir {
			numDirs++
		}

		return nil
	})
	if err != nil {
		return err
	}

	logger := log.WithField("dataDir", dataDir)
	if dir != dataDir {
		logger = logger.WithField("path", dir)
	}
	logger.Infof("Data directory clean (%d directories scanned)", numDirs)
	return nil
}

func validateFile(path string, d fs.DirEntry, quick bool) error {
	b, err := os.ReadFile(path)
	if err != nil {
		return err
	}

	log.WithField("file", path).Debug("Validating file")

	if quick && d.Name() != PropertiesFile {
		return nil
	}

	switch d.Name() {
	case DependenciesFile:
		return json.Unmarshal(b, &[]Dependency{})
	case GroupMetadataFile:
		return json.Unmarshal(b, &gitlab.Group{})
	case ProjectMetadataFile:
		return json.Unmarshal(b, &Project{})
	case PropertiesFile:
		p := Properties{}
		if err := yaml.Unmarshal(b, &p); err != nil {
			return err
		}
		for _, d := range p.Dependencies {
			if d.VersionExtract != nil {
				if _, err := regexp.Compile(d.VersionExtract.Regexp); err != nil {
					return fmt.Errorf("Dependency %q: %v", d.Name, err)
				}
			}
		}
	case VulnerabilitiesFile:
		return yaml.Unmarshal(b, &[]Vulnerability{})
	case CIConfigFile:
		return json.Unmarshal(b, &gitlab.ProjectLintResult{})
	case ViolationsFile:
		return json.Unmarshal(b, &OPAViolations{})
	case ProtectedBranchesFile:
		return json.Unmarshal(b, &[]*gitlab.ProtectedBranch{})
	case IgnoreFile, "README.md", ".gitkeep", ".DS_Store":
	default:
		return fmt.Errorf("Unexpected filename %q", d.Name())
	}
	return nil
}
