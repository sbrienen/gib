package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"

	"github.com/xanzy/go-gitlab"
	"golang.org/x/sync/errgroup"

	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"sigs.k8s.io/yaml"
)

// Project is a wrapper around *gitlab.Project
type Project struct {
	*gitlab.Project
	Dependencies      []Dependency              `json:"-"`
	Properties        *Properties               `json:"-"`
	StoragePath       string                    `json:"-"`
	Vulnerabilities   []Vulnerability           `json:"-"`
	CIConfig          *gitlab.ProjectLintResult `json:"-"`
	ProtectedBranches []*gitlab.ProtectedBranch `json:"-"`
	// Remove fields
	ForksCount                omit `json:"forks_count,omitempty"`
	StarCount                 omit `json:"star_count,omitempty"`
	LastActivityAt            omit `json:"last_activity_at,omitempty"`
	OpenIssuesCount           omit `json:"open_issues_count,omitempty"`
	ContainerExpirationPolicy omit `json:"container_expiration_policy,omitempty"`
	RunnersToken              omit `json:"runners_token,omitempty"`
}

type omit *struct{}

func (p Project) logger() *logrus.Entry {
	return log.WithFields(log.Fields{
		"name": p.Name,
		"path": p.PathWithNamespace,
	})
}

// Save creates or update all the files of the project p
func (p *Project) Save() error {
	logger := p.logger()

	// Create projectDir if doesn't exist
	if _, err := os.Stat(p.StoragePath); os.IsNotExist(err) {
		logger.Info("New project created")
		err := os.Mkdir(p.StoragePath, 0700)
		if err != nil {
			return err
		}
	}

	logger.Info("Updating project")
	g := new(errgroup.Group)
	g.Go(p.storeMetadata)
	g.Go(p.storeDependencies)
	g.Go(p.storeVulnerabilities)
	g.Go(p.storeCIConfig)
	g.Go(p.storeProtectedBranches)

	return g.Wait()
}

func (p Project) storeMetadata() error {
	// save project metadata
	pjson, err := json.MarshalIndent(p, "", "   ")
	if err != nil {
		return err
	}
	return os.WriteFile(filepath.Join(p.StoragePath, ProjectMetadataFile), pjson, 0600)
}

func (p Project) storeDependencies() error {
	if len(p.Dependencies) == 0 {
		return nil // noop
	}

	deps, err := json.Marshal(p.Dependencies)
	if err != nil {
		return err
	}
	return os.WriteFile(filepath.Join(p.StoragePath, DependenciesFile), deps, 0600)
}

func (p Project) storeVulnerabilities() error {
	if len(p.Vulnerabilities) == 0 {
		return nil // noop
	}

	vulns, err := json.Marshal(p.Vulnerabilities)
	if err != nil {
		return err
	}
	return os.WriteFile(filepath.Join(p.StoragePath, VulnerabilitiesFile), vulns, 0600)
}

func (p Project) storeCIConfig() error {
	if p.CIConfig == nil {
		return nil // noop
	}

	ciConfig, err := json.Marshal(p.CIConfig)
	if err != nil {
		return err
	}
	return os.WriteFile(filepath.Join(p.StoragePath, CIConfigFile), ciConfig, 0600)
}

func (p Project) storeProtectedBranches() error {
	if p.ProtectedBranches == nil {
		return nil // noop
	}

	pb, err := json.Marshal(p.ProtectedBranches)
	if err != nil {
		return err
	}
	return os.WriteFile(filepath.Join(p.StoragePath, ProtectedBranchesFile), pb, 0600)
}

// LoadProperties reads the project's properties.yml file, and loads it into the current project
func (p *Project) LoadProperties() error {
	p.Properties = &Properties{}
	propFile := filepath.Join(p.StoragePath, PropertiesFile)
	if _, err := os.Stat(propFile); os.IsNotExist(err) {
		return nil
	}
	b, err := os.ReadFile(propFile)
	if err != nil {
		return err
	}
	if string(b) == "" {
		return fmt.Errorf("Properties file %q is empty", propFile)
	}
	return yaml.Unmarshal(b, &p.Properties)
}

// Sync sends a request to the GitLab API to get the project metadata
func (p *Project) Sync(git *gitlab.Client) error {
	if p.ID == 0 {
		p.logger().Info("Syncing project")
		project, _, err := git.Projects.GetProject(p.PathWithNamespace, &gitlab.GetProjectOptions{}, nil)
		if err != nil {
			return err
		}
		p.Project = project
	}

	err := p.LoadProperties()
	if err != nil {
		return err
	}

	if p.isAProductProject() {
		g := new(errgroup.Group)
		ch := make(chan interface{}, 100)

		g.Go(func() error { return p.downloadDependencies(git, ch) })
		g.Go(func() error { return p.downloadVulnerabilities(git, ch) })
		g.Go(func() error { return p.downloadCIConfig(git, ch) })
		g.Go(func() error { return p.downloadProtectedBranches(git, ch) })
		go func() {
			g.Wait() // nolint
			close(ch)
		}()

		for result := range ch {
			switch t := result.(type) {
			case []Vulnerability:
				p.Vulnerabilities = append(p.Vulnerabilities, result.([]Vulnerability)...)
			case []Dependency:
				p.Dependencies = append(p.Dependencies, result.([]Dependency)...)
			case *gitlab.ProjectLintResult:
				p.CIConfig = result.(*gitlab.ProjectLintResult)
			case []*gitlab.ProtectedBranch:
				p.ProtectedBranches = result.([]*gitlab.ProtectedBranch)
			default:
				return fmt.Errorf("Unknown type downloaded: %q", t)
			}
		}

		if err := g.Wait(); err != nil {
			return err
		}
		p.logger().Infof("Downloaded %d dependencies", len(p.Dependencies))
		if err := p.mergeDependencies(git); err != nil {
			return err
		}
		p.logger().Infof("Downloaded %d vulnerabilities", len(p.Vulnerabilities))
		p.logger().Infof("Downloaded %d protected branches", len(p.ProtectedBranches))
	}

	return p.Save()
}

func (p Project) isAProductProject() bool {
	for _, cat := range p.Properties.Categories {
		if cat == CategoryProduct {
			return true
		}
	}
	return false
}

func (p Project) downloadDependencies(git *gitlab.Client, ch chan interface{}) error {
	p.logger().Info("Downloading dependencies")

	// The number of pages isn't known in advance, at least one request must be done to get this value.
	// This function gets the dependencies for a specific page.
	dl := func(page int) ([]Dependency, *gitlab.Response, error) {
		var dependencies []Dependency

		listOpts := gitlab.ListOptions{
			PerPage: 100,
			Page:    page,
		}

		u := fmt.Sprintf("projects/%s/dependencies", strings.Replace(url.PathEscape(p.PathWithNamespace), ".", "%2E", -1))
		req, err := git.NewRequest(http.MethodGet, u, listOpts, nil)
		if err != nil {
			return dependencies, nil, err
		}

		response, err := git.Do(req, &dependencies)
		return dependencies, response, err
	}

	deps, response, err := dl(1)
	if err != nil {
		return err
	}
	ch <- deps

	pages := response.TotalPages
	nextPage := 0
	// If a query returns more than 10,000 record, x-total-pages isn't returned and defaults to 0
	if pages == 0 {
		pages = 99
		nextPage = 100 // We need the last page response to get the next page (if exists)
	}

	// Download pages asynchronously
	g := new(errgroup.Group)

	for page := 2; page < pages+1; page++ {
		page := page // https://go.dev/doc/faq#closures_and_goroutines
		g.Go(func() error {
			if page%10 == 0 {
				p.logger().Debugf("Downloading dependencies page %d", page)
			}
			deps, _, err := dl(page)
			ch <- deps
			return err
		})
	}

	// Download remaining pages
	for nextPage != 0 {
		if nextPage%10 == 0 {
			p.logger().Debugf("Downloading dependencies page %d", nextPage)
		}
		deps, response, err := dl(nextPage)
		if err != nil {
			return err
		}
		ch <- deps
		nextPage = response.NextPage
	}

	// Wait for the concurrent requests to finish
	return g.Wait()
}

func (p Project) downloadVulnerabilities(git *gitlab.Client, ch chan interface{}) error {
	p.logger().Info("Downloading vulnerabilities")

	// The number of pages isn't known in advance, at least one request must be done to get this value.
	// This function gets the vulnerabilities for a specific page.
	dl := func(page int) ([]Vulnerability, *gitlab.Response, error) {
		var vulnerabilities []Vulnerability

		listOpts := gitlab.ListOptions{
			PerPage: 100,
			Page:    page,
		}

		u := fmt.Sprintf("projects/%s/vulnerabilities", strings.Replace(url.PathEscape(p.PathWithNamespace), ".", "%2E", -1))
		req, err := git.NewRequest(http.MethodGet, u, listOpts, nil)
		if err != nil {
			return vulnerabilities, nil, err
		}

		response, err := git.Do(req, &vulnerabilities)
		return vulnerabilities, response, err
	}

	vulns, response, err := dl(1)
	if err != nil {
		return err
	}
	ch <- vulns
	pages := response.TotalPages
	nextPage := 0
	// If a query returns more than 10,000 record, x-total-pages isn't returned and defaults to 0
	if pages == 0 {
		pages = 99
		nextPage = 100
	}

	// Download pages asynchronously
	g := new(errgroup.Group)

	for page := 2; page < pages+1; page++ {
		page := page // https://go.dev/doc/faq#closures_and_goroutines
		g.Go(func() error {
			if page%10 == 0 {
				p.logger().Debugf("Downloading vulnerabilities page %d", page)
			}
			vulns, _, err := dl(page)
			if err != nil {
				return err
			}
			ch <- vulns
			return nil
		})
	}

	// Download remaining pages
	for nextPage != 0 {
		if nextPage%10 == 0 {
			p.logger().Debugf("Downloading vulnerabilities page %d", nextPage)
		}
		vulns, response, err := dl(nextPage)
		if err != nil {
			return err
		}
		ch <- vulns
		nextPage = response.NextPage
	}

	// Wait for the concurrent requests to finish
	return g.Wait()
}

func (p Project) downloadCIConfig(git *gitlab.Client, ch chan interface{}) error {
	p.logger().Info("Downloading CI configuration")
	CIConfig, _, err := git.Validate.ProjectLint(p.PathWithNamespace, &gitlab.ProjectLintOptions{})
	ch <- CIConfig
	p.logger().Info("Downloaded CI Configuration")
	return err
}

func (p Project) downloadProtectedBranches(git *gitlab.Client, ch chan interface{}) error {
	p.logger().Info("Downloading protected branches")

	// The number of pages isn't known in advance, at least one request must be done to get this value.
	// This function gets the protected branches for a specific page.
	dl := func(page int) ([]*gitlab.ProtectedBranch, *gitlab.Response, error) {
		return git.ProtectedBranches.ListProtectedBranches(p.PathWithNamespace, &gitlab.ListProtectedBranchesOptions{PerPage: 100, Page: 1})
	}
	pBranches, response, err := dl(1)

	if err != nil {
		return err
	}
	ch <- pBranches

	pages := response.TotalPages
	nextPage := 0
	// If a query returns more than 10,000 record, x-total-pages isn't returned and defaults to 0
	if pages == 0 {
		pages = 99
		nextPage = 100 // We need the last page response to get the next page (if exists)
	}

	// Download pages asynchronously
	g := new(errgroup.Group)

	for page := 2; page < pages+1; page++ {
		page := page // https://go.dev/doc/faq#closures_and_goroutines
		g.Go(func() error {
			if page%10 == 0 {
				p.logger().Debugf("Downloading protected_branches page %d", page)
			}
			pBranches, _, err := dl(page)
			ch <- pBranches
			return err
		})
	}

	// Download remaining pages
	for nextPage != 0 {
		if nextPage%10 == 0 {
			p.logger().Debugf("Downloading protected_branches page %d", nextPage)
		}
		pBranches, response, err := dl(nextPage)
		if err != nil {
			return err
		}
		ch <- pBranches
		nextPage = response.NextPage
	}

	// Wait for the concurrent requests to finish
	return g.Wait()
}

// mergeDependencies merges dependencies from Properties into the project dependencies
// See https://gitlab.com/gitlab-com/gl-security/engineering-and-research/gib/-/issues/27
func (p *Project) mergeDependencies(git *gitlab.Client) error {
	if len(p.Properties.Dependencies) == 0 {
		return nil
	}

	p.logger().Infof("Merging %d dependencies from Properties", len(p.Properties.Dependencies))
	for _, d := range p.Properties.Dependencies {
		if err := d.FetchVersion(git, p.PathWithNamespace, p.DefaultBranch); err != nil {
			return err
		}
		p.Dependencies = append(p.Dependencies, d)
	}
	return nil
}
