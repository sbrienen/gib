package main

import (
	"fmt"
	"io/fs"
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"github.com/xanzy/go-gitlab"
)

const (
	// ProjectMetadataFile is the path of the projects metadata json file
	ProjectMetadataFile = "project.json"
	// GroupMetadataFile is the path of the groups metadata json file
	GroupMetadataFile = "group.json"
	// IgnoreFile is the name of the ignore files
	IgnoreFile = "ignore"
	// DependenciesFile is the name of the dependency report of a project
	DependenciesFile = "dependencies.json"
	// VulnerabilitiesFile is the name of the vulnerability report of a project
	VulnerabilitiesFile = "vulnerabilities.json"
	// CIConfigFile is the name of the CI configuration file of a project
	CIConfigFile = "ci-config.json"
	// ViolationsFile is the output of `opa eval`, with policy violations
	ViolationsFile = "violations.json"
	// ProtectedBranchesFile is the name of the protected branches file for a project
	ProtectedBranchesFile = "protected_branches.json"
)

func syncCmd(c *cli.Context) error {
	git := newClient(c)

	// Walk the data tree
	dataDir := c.String(dataDirFlagName)
	dir := c.Args().First()
	if dir == "" {
		dir = dataDir
	}
	err := walkDataDir(dir, dataDir, git)
	if err != nil {
		return fmt.Errorf("error walking the path %q: %v", dataDir, err)
	}
	return nil
}

func walkDataDir(dir, dataDir string, git *gitlab.Client) error {
	if dir == "" {
		dir = dataDir
	}
	return filepath.WalkDir(dir, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		// Groups and projects are always directories
		if !d.IsDir() ||
			// Root of dataDir is a noop
			path == dataDir {
			return nil
		}

		files, err := os.ReadDir(path)
		if err != nil {
			return err
		}

		if path == dir && isProject(d, files) {
			projectNS, err := filepath.Rel(dataDir, path)
			if err != nil {
				return err
			}

			project := &Project{
				StoragePath: path,
				Project: &gitlab.Project{
					PathWithNamespace: projectNS,
				},
			}
			return project.Sync(git)
		}

		if !isGroup(d, files) {
			return nil
		}

		return syncPath(path, dataDir, git)
	})
}

func hasIgnoreFile(files []fs.DirEntry) bool {
	for _, f := range files {
		if !f.IsDir() && f.Name() == IgnoreFile {
			return true
		}
	}
	return false
}

func cleanupProjects(projects []*Project, path string) error {
	files, err := os.ReadDir(path)
	if err != nil {
		log.Fatal(err)
	}

	// clean-up projects
CLEANUP:
	for _, f := range files {
		projectDir := filepath.Join(path, f.Name())
		if !f.IsDir() {
			continue
		}
		projectFiles, err := os.ReadDir(projectDir)
		if err != nil {
			return err
		}

		if !isProject(f, projectFiles) {
			continue
		}

		for _, project := range projects {
			if f.Name() == project.Path {
				continue CLEANUP
			}
		}

		log.WithFields(log.Fields{
			"name": f.Name(),
			"path": f.Name(),
		}).Info("Removing deleted or archived project")
		err = os.RemoveAll(projectDir)
		if err != nil {
			return err
		}
	}
	return nil
}

func isProject(d fs.DirEntry, files []fs.DirEntry) bool {
	if !d.IsDir() {
		return false
	}
	for _, f := range files {
		if !f.IsDir() && (f.Name() == ProjectMetadataFile || f.Name() == PropertiesFile) {
			return true
		}
	}

	return false
}

func isGroup(d fs.DirEntry, files []fs.DirEntry) bool {
	if !d.IsDir() {
		return false
	}
	return !isProject(d, files)
}

func syncPath(path, dataDir string, git *gitlab.Client) error {
	files, err := os.ReadDir(path)
	if err != nil {
		return err
	}

	group, err := filepath.Rel(dataDir, path)
	if err != nil {
		return err
	}

	projects, err := updateGroupProjects(path, dataDir, files, group, git)
	if err != nil {
		return err
	}

	err = cleanupProjects(projects, path)
	if err != nil {
		return err
	}

	if !hasIgnoreFile(files) {
		err := updateSubGroups(path, group, git)
		if err != nil {
			return err
		}
	}
	return nil
}

func updateSubGroups(path, group string, git *gitlab.Client) error {
	log.WithField("group", group).Info("Fetching subgroups")

	// Update subgroups
	page := 1
	var subgroups []*Group
	for page != 0 {
		listOpts := &gitlab.ListSubgroupsOptions{
			ListOptions: gitlab.ListOptions{
				PerPage: 100,
				Page:    page,
			},
		}

		s, response, err := git.Groups.ListSubgroups(group, listOpts)
		if err != nil {
			return err
		}

		page = response.NextPage
		for _, subgroup := range s {
			subgroups = append(subgroups, &Group{StoragePath: filepath.Join(path, subgroup.Path), Group: subgroup})
		}
	}

	log.WithFields(log.Fields{
		"group": group,
	}).Infof("Found %d subgroup(s)", len(subgroups))

	for _, subgroup := range subgroups {
		err := subgroup.Save()
		if err != nil {
			return err
		}
	}

	return cleanupGroups(path, subgroups)
}

func cleanupGroups(path string, subgroups []*Group) error {
	// clean-up groups
	files, err := os.ReadDir(path)
	if err != nil {
		log.Fatal(err)
	}

CLEANUP:
	for _, f := range files {
		if !f.IsDir() {
			continue
		}

		groupDir := filepath.Join(path, f.Name())
		groupFiles, err := os.ReadDir(groupDir)
		if err != nil {
			return err
		}

		if !isGroup(f, groupFiles) {
			continue
		}

		for _, group := range subgroups {
			if f.Name() == group.Path {
				continue CLEANUP
			}
		}

		log.WithFields(log.Fields{
			"path":  groupDir,
			"group": f.Name(),
		}).Info("Removing deleted group")
		err = os.RemoveAll(groupDir)
		if err != nil {
			return err
		}
	}
	return nil
}

func updateGroupProjects(path, dataDir string, files []fs.DirEntry, group string, git *gitlab.Client) ([]*Project, error) {
	var projects []*Project

	if hasIgnoreFile(files) {
		// If the current folder has an ignore file, we need to check if some projects
		// are present, and sync them one by one.
		for _, f := range files {
			if !f.IsDir() {
				continue
			}
			projectPath := filepath.Join(path, f.Name())
			pFiles, err := os.ReadDir(projectPath)
			if err != nil {
				return projects, err
			}

			if isProject(f, pFiles) {
				projectNS, err := filepath.Rel(dataDir, projectPath)
				if err != nil {
					return projects, err
				}
				project := &Project{
					StoragePath: projectPath,
					Project: &gitlab.Project{
						PathWithNamespace: projectNS,
					},
				}
				projects = append(projects, project)
			}
		}
	} else {
		// Otherwise, we fetch all the projects of the current group
		log.WithField("group", group).Info("Fetching group projects")

		page := 1
		for page != 0 {
			listOpts := &gitlab.ListGroupProjectsOptions{
				WithShared: gitlab.Bool(false),
				ListOptions: gitlab.ListOptions{
					PerPage: 100,
					Page:    page,
				},
			}

			p, response, err := git.Groups.ListGroupProjects(group, listOpts)
			if err != nil {
				return projects, err
			}
			page = response.NextPage
			for _, project := range p {
				if project.Archived {
					log.WithFields(log.Fields{
						"name":  project.Name,
						"path":  project.Path,
						"group": group,
					}).Debug("Skipping archived project")
					continue
				}
				projects = append(projects, &Project{StoragePath: filepath.Join(path, project.Path), Project: project})
			}
		}
		log.WithField("group", group).Infof("Found %d project(s)", len(projects))
	}

	for _, project := range projects {
		err := project.Sync(git)
		if err != nil {
			return projects, err
		}
	}
	return projects, nil
}
