package main

import (
	"encoding/base64"
	"fmt"
	"regexp"

	"github.com/xanzy/go-gitlab"
)

// Dependency is a project dependency
// See https://docs.gitlab.com/ee/api/dependencies.html
// Note: `Vulnerabilities` is omitted on purpose
// Dependencies can be added/overriden in properties.yml files
type Dependency struct {
	Name           string `json:"name"`
	Version        string `json:"version"`
	VersionExtract *struct {
		FilePath string `json:"file_path"`
		Ref      string `json:"ref"`
		Regexp   string `json:"regexp"`
	} `json:"version_extract,omitempty"`
	PackageManager     string `json:"package_manager"`
	DependencyFilePath string `json:"dependency_file_path"`
	// If set, PathWithNamespace is the path of the source code project of the dependency in the inventory
	PathWithNamespace string `json:"path_with_namespace,omitempty"`
}

// FetchVersion extract the version from the FilePath in the repository.
func (d *Dependency) FetchVersion(git *gitlab.Client, pid string, defaultBranch string) error {
	if d.VersionExtract == nil {
		return nil
	}

	re, err := regexp.Compile(d.VersionExtract.Regexp)
	if err != nil {
		return fmt.Errorf("Error compiling version_extract regexp (%q) for dependency %s", d.VersionExtract.Regexp, d.Name)
	}

	if d.VersionExtract.Ref == "" {
		d.VersionExtract.Ref = defaultBranch
	}

	file, _, err := git.RepositoryFiles.GetFile(pid, d.VersionExtract.FilePath, &gitlab.GetFileOptions{Ref: gitlab.String(d.VersionExtract.Ref)})
	if err != nil {
		return err
	}
	content, err := base64.StdEncoding.DecodeString(file.Content)
	if err != nil {
		return err
	}

	version := re.FindStringSubmatch(string(content))
	if len(version) == 0 {
		return fmt.Errorf("version_extract did not match any version with regexp (%q) for dependency %s", d.VersionExtract.Regexp, d.Name)
	}
	d.Version = string(version[1])

	d.VersionExtract = nil

	return nil
}
