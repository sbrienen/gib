ARG GO_VERSION=1.17
FROM golang:$GO_VERSION-alpine AS builder

ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /src

# Cache go dependencies
COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY . ./
RUN go build -v -ldflags="-s -w" -o /bin/gib

FROM mikefarah/yq:4 as yq

FROM debian:stable-slim

ARG HUGO_VERSION
ENV HUGO_VERSION ${HUGO_VERSION:-0.92.0}

ARG SQLITE_UTILS_VERSION
ENV SQLITE_UTILS_VERSION ${SQLITE_UTILS_VERSION:-3.22.0}

ADD https://www.openpolicyagent.org/downloads/latest/opa_linux_amd64_static /usr/local/bin/opa
ADD https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_Linux-64bit.deb /tmp
RUN apt-get update && apt-get install -y --no-install-recommends \
      python3-pip jq ca-certificates git git-lfs sqlite3 p7zip-full \
      && rm -rf /var/lib/apt/lists/* && \

    pip install --no-cache-dir sqlite-utils==${SQLITE_UTILS_VERSION} && \

    dpkg -i /tmp/hugo_${HUGO_VERSION}_Linux-64bit.deb && \

    chmod +x /usr/local/bin/opa && \

    groupadd --gid 1000 gib && \
    useradd --uid 1000 --gid gib --shell /bin/bash --create-home gib
COPY --from=builder /bin/gib /usr/local/bin/
COPY --from=yq /usr/bin/yq /usr/local/bin/
COPY policies/ /usr/local/share/policies/
COPY reports/theme/ /usr/local/share/themes/gitlab/

USER gib

ENTRYPOINT ["/usr/local/bin/gib"]
