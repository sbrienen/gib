package main

import (
	"os"
	"path/filepath"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
)

func main() {
	app := NewApp()
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

const (
	apiTokenFlagName   = "api-token"
	usernameFlagName   = "username"
	passwordFlagName   = "password"
	apiBaseURLFlagName = "api-baseurl"
	dataDirFlagName    = "data-dir"
)

var gitlabAPIFlags = []cli.Flag{
	&cli.StringFlag{
		Name:    usernameFlagName,
		Aliases: []string{"u"},
		Usage:   "Username for Basic Auth",
		EnvVars: []string{"GITLAB_USERNAME"},
	},
	&cli.StringFlag{
		Name:    passwordFlagName,
		Aliases: []string{"p"},
		Usage:   "Password for Basic Auth",
		EnvVars: []string{"GITLAB_PASSWORD"},
	},
	&cli.StringFlag{
		Name:    apiTokenFlagName,
		Aliases: []string{"t"},
		Usage:   "Personal/Project Access Token",
		EnvVars: []string{"GITLAB_API_TOKEN"},
	},
	&cli.StringFlag{
		Name:    apiBaseURLFlagName,
		Value:   "https://gitlab.com/",
		Usage:   "Base URL of the GitLab instance",
		EnvVars: []string{"GITLAB_API_BASEURL"},
	},
}

// NewApp returns a *cli.App with all commands and flags configured
func NewApp() *cli.App {
	app := cli.NewApp()
	app.Authors = []*cli.Author{&cli.Author{Name: "GitLab"}}
	app.Compiled = time.Now()
	app.HelpName = "gib"
	app.Usage = "GitLab Inventory Builder"
	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:    "log-level",
			Aliases: []string{"l"},
			Usage:   "panic|fatal|error|warn|info|debug|trace",
			Value:   "info",
			EnvVars: []string{"LOGLEVEL"},
		},
		&cli.StringFlag{
			Name:    dataDirFlagName,
			Usage:   "Folder to store projects data",
			Value:   "./data",
			EnvVars: []string{"DATA_DIR_PATH"},
		},
	}

	app.Before = func(c *cli.Context) error {
		level, err := log.ParseLevel(c.String("log-level"))
		if err != nil {
			return err
		}
		log.SetLevel(level)
		log.SetOutput(c.App.Writer)
		return nil
	}

	app.Commands = []*cli.Command{
		{
			Name:    "compliance",
			Aliases: []string{"c"},
			Usage:   "Compliance (policies)",
			Subcommands: []*cli.Command{
				{
					Name:      "run",
					Usage:     "Run Compliance (OPA) script",
					ArgsUsage: "[subdirectory of DATA_DIR_PATH]",
					Action:    runCompliance,
				},
				{
					Name:   "export",
					Usage:  "Export violation from the SQLite DB",
					Action: exportViolationsCmd,
				},
				{
					Name:  "create-issues",
					Usage: "Create issues for each violation (idempotent)",
					Flags: append(gitlabAPIFlags, []cli.Flag{
						&cli.StringFlag{
							Name:    "project-id",
							Usage:   "Project ID",
							EnvVars: []string{"PROJECT_ID", "CI_PROJECT_ID"},
						},
						&cli.StringFlag{
							Name:    "violations-file",
							Usage:   "Violations (see the export command) file path",
							Value:   filepath.Join("tmp", "violations.json"),
							EnvVars: []string{"VIOLATIONS_FILE"},
						},
					}...),
					Action: createIssuesForViolationsCmd,
				},
			},
		},
		{
			Name:    "generate-reports",
			Aliases: []string{"g"},
			Usage:   "Generate static HTML reports",
			Action:  generateReportsCmd,
		},
		{
			Name:      "sync",
			Aliases:   []string{"s"},
			Usage:     "Sync local data using the GitLab API",
			ArgsUsage: "[subdirectory of DATA_DIR_PATH]",
			Flags:     gitlabAPIFlags,
			Action:    syncCmd,
		},
		{
			Name:    "update-db",
			Aliases: []string{"u"},
			Usage:   "Generate or update a local sqlite3 DB reflecting the data folder",
			Action:  updateDBCmd,
		},
		{
			Name:      "validate",
			Aliases:   []string{"v"},
			Usage:     "Validate local data",
			ArgsUsage: "[subdirectory of DATA_DIR_PATH]",
			Flags: []cli.Flag{
				&cli.BoolFlag{
					Name:    "quick",
					Aliases: []string{"q"},
					Value:   false,
					Usage:   "Quick validation (only properties.yml files)",
					EnvVars: []string{"QUICK_VALIDATION"},
				},
			},
			Action: validateCmd,
		},
	}

	return app
}
