# GitLab Inventory Builder

The GitLab Inventory Build (`gib`) is a tool to generate and maintain a complete inventory of
projects (hosted on GitLab.com or Self-Managed) with their dependencies.

The project was built with the following constraints in mind:

- Easy and lightweight usage (no daemon)
- No dependency on external services, other than GitLab itself
- Easy to query data
- Flexible reports generation
- Can work with gitlab.com or self-hosted instances

## Usage

To build an inventory, create a new empty project, and add a `.gitlab-ci.yml` containing:

```yaml
include:
  remote: 'https://gitlab.com/gitlab-com/gl-security/engineering-and-research/gib/-/raw/main/ci/Inventory-Builder.gitlab-ci.yml'
```

This GitLab-CI template defines variables that you can
[override](https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence) in your project if
needed.

The next step is to [configure](#configuration) your project, and run a pipeline.  `gib` will sync
your data and create merge requests to let you review the changes.

Reports are also generated and available as pipeline artifacts.

It is recommended to run [scheduled](https://docs.gitlab.com/ee/ci/pipelines/schedules.html)
pipelines (for example once a week) to keep the inventory in sync automatically.

### Configuration

The `data` folder (default for `$DATA_DIR_PATH`) is used as configuration (see [the folders section
below](#folders)). To scan groups, add them at the root of this folder.

```
.
└── data
    ├── gitlab-com
    └── gitlab-org
        ├── memory-team
        │   └── ignore
        └── security
            └── ignore
```

This layout will sync the projects and subgroups under `gitlab-com` and `gitlab-org`.
The ones under `gitlab-org/memory-team` and `gitlab-org/security` will be ignored.

`gib` will save a `group.json` data file in every subgroup folder being synced. This file is created
from the upper group [subgroups
data](https://docs.gitlab.com/ee/api/groups.html#list-a-groups-subgroups). The same goes for their
[projects](https://docs.gitlab.com/ee/api/groups.html#list-a-groups-projects), with a `project.json`
file for each project.

#### Directory traversal

In the following example, the subgroups and projects or `gitlab-org` won't be synced, but since we
created the `shared-runners` folder, this one will be synced.

```
.
└── data
    └── gitlab-org
        ├── ignore
        └── shared-runners
            ├── images
            ├── homebrew
            └── macos
```

When running `gib sync` on this data folder, the result will be:

```
.
  └── data
      └── gitlab-org
        ├── ignore
        └── shared-runners
            ├── images
            │   └── project.json
            ├── gcp
            │   └── group.json
            │   └── windows-containers
            │       └── project.json
            ├── macstadium
            │   └── group.json
            │   └── orka
            │       └── project.json
            ├── homebrew
            │   └── project.json
            └── macos
                └── project.json
```

(based on current data as we're writing this documentation).

#### Standalone projects

In the following example, the subgroups and projects or `gitlab-org` won't be synced, but since we
created the `ci-cd/docker-machine` folder with a `properties.yml` file, `gib` knows it's a project.

```
.
└── data
    └── gitlab-org
        ├── ignore
        └── ci-cd
            ├── ignore
            └── docker-machine
                └── properties.yml
```

When running `gib sync` on this data folder, the result will be:

```
.
└── data
    └── gitlab-org
        ├── ignore
        └── ci-cd
            ├── ignore
            └── docker-machine
                ├── project.json
                └── properties.yml
```

Only the `docker-machine` project has been synced. This layout is useful when you don't want to sync
all the projects under a group, but it comes at a price: these projects are synced one by one,
instead of batches (of 100) when in a subgroup.

#### GitLab API Permissions

In order to sync your inventory, and create Merge Requests for changes, `gib` needs an authorized
access to the GitLab API. A [Personal Access
Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with the `api` scope can
be used by setting the `$GITLAB_API_TOKEN` variable.

### Sync projects data

`gib sync` will sync the `$DATA_DIR_PATH` by connecting to the GitLab API (`$GITLAB_API_BASEURL`).

```sh
$ docker run -it --rm \
  -v $PWD/data:/data \
  -e GITLAB_API_TOKEN \
  registry.gitlab.com/gitlab-com/gl-security/engineering-and-research/gib sync
```

#### Sync a sub-tree

To update a portion of `$DATA_DIR_PATH` only, the first argument passed to `gib sync` lets you specify
a folder under `$DATA_DIR_PATH`. The folder can be a group or a single project, for example:

```sh
$ docker run -it --rm \
  -v $PWD/data:/data \
  -e GITLAB_API_TOKEN \
  registry.gitlab.com/gitlab-com/gl-security/engineering-and-research/gib sync ./data/gitlab-org/gitlab-services/design.gitlab.com
```

will sync only the project `gitlab-org/gitlab-services/design.gitlab.com`.

### Validate projects data

While the `$DATA_DIR_PATH` is being synced automatically by `gib`, its configuration is mostly done by
users, and therefore it must be validated. The command can be used prior to committing files like
`properties.yml` to your inventory repository.


```sh
$ docker run -it --rm \
  -v $PWD/data:/data \
  registry.gitlab.com/gitlab-com/gl-security/engineering-and-research/gib validate
```

The command validates the name of files in `$DATA_DIR_PATH`, among files [used by gib](#files_used_by_gib)
and [permitted](#permitted_files). The content of the [used by gib](#files_used_by_gib) is also
validated.

### Generate HTML reports

Gib can generate HTML reports with the data of your inventory. These reports provide useful metrics
on vulnerabilities (for `product` projects).


```sh
$ docker run -it --rm \
  -v $PWD:/inventory \
  -w /inventory \
  -e GITLAB_API_TOKEN \
  registry.gitlab.com/gitlab-com/gl-security/engineering-and-research/gib generate-reports
```

The `GITLAB_API_TOKEN` is required to fetch user data for the reports. Once the command executed,
the reports are available in the `public/` folder of your inventory.

### Self-hosted GitLab

By default, `gib` will use the gitlab.com API located at https://gitlab.com/api/v4. `gib` can be
used with a different API URL by setting the `$GITLAB_API_BASEURL` variable. If several hosts must
be used, run `gib` multiple times with different `data` folders and API URLs.

### Compliance

The [CI/CD template](ci/Inventory-Builder.gitlab-ci.yml) has a `compliance` stage, along with an
`opa` job. This job evaluates projects and sites for violations (see [this Merge
Request](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/83315/) for details on
GitLab's default polices).

If the local inventory contains the following directories:
- polices/projects/
- policies/sites/
- policies/vulnerabilities/

they will be used in lieu of the [default ones](policies/) shipping with Gib. Don't forget to set
the right variables for the queries as well when using the CI/CD template.

To find violations, run:

$ docker run -it --rm \
  -v $PWD:/inventory \
  -w /inventory \
  registry.gitlab.com/gitlab-com/gl-security/engineering-and-research/gib compliance run


#### Create issues

By default, `gib` will create issues for the violations found during the `compliance` stage. One
issue is created by violation. Scoped labels with the project ID and a key for the violation are
applied to the issue. To execute this feature locally, you need to run 2 commands, the first is
needed to export the violations to a temporary json file.

```sh
$ docker run -it --rm \
  -v $PWD:/inventory \
  -w /inventory \
  registry.gitlab.com/gitlab-com/gl-security/engineering-and-research/gib compliance export
$ docker run -it --rm \
  -v $PWD:/inventory \
  -w /inventory \
  -e GITLAB_API_TOKEN \
  -e PROJECT_ID=123 \
  registry.gitlab.com/gitlab-com/gl-security/engineering-and-research/gib compliance create-issues
```

When violations are not reported anymore, the corresponding issue will be labeled as `~resolved`. It
is the responsibility of the user to close the issue after verification. If the violation is
detected again, the label is removed, and the violation marked as `Reopened`.

Closed issues for reported violations will be reopen automatically. The corresponding violation will
have a `Reopened` badged next to them in the reports. To keep them closed see the next section to
ignore some violations.

#### Ignoring some violations

Not all violations are relevant, and it's sometimes useful to ignore them to limit the noise. To
ignore a violation, apply the `~ignore` label to the corresponding issue. The issue can then be
closed, and will stay closed. The violation will still appear in the reports, with an `Ignored`
badge next to them.

### Search for dependencies

All dependencies fetched are stored in the local SQLite database generated by the `update-db` CI/CD job.

To search for dependencies in your projects:

- Download the compressed DB file from `db/inventory.db.7z` in your inventory repo
- Extract the database with [7z](https://www.7-zip.org/download.html): `7z x db/inventory.db.7z`
- Run this query on the database:
`PACKAGE=a_package; sqlite3 db/inventory.db "select d.name, d.version, p.path_with_namespace from dependencies as d join projects p on d.project_id = p.id where lower(d.name) like '${PACKAGE}';"`

### Manually add dependencies

Dependencies are fetched from the [GitLab API](https://docs.gitlab.com/ee/api/dependencies.html),
which is relying on [Dependency Scanning reports]. The list of dependencies in the inventory can be
augmented with the `properties.yml` file, and the `dependencies` keyword. `dependencies` is an array
of [`Dependency`](dependencies.go).

[Dependency Scanning reports]: https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsdependency_scanning

Example:

```yaml
categories:
  - product

dependencies:
  - name: "bitnami/jruby"
    version: "latest"
    package_manager: "docker"
    dependency_file_path: "Dockerfile:18"
    path_with_namespace: "gitlab-org/containers/bitnami/jruby"
```

`package_manager` is not enforcing specific values, and can be set to anything.
`path_with_namespace` (optional) is a key specific to this inventory. It links the dependency with a project of
the inventory. If set, it must be the `path_with_namespace` of the dependency project to allow
cross-linking.

#### Extract version

Because declared dependencies become static when being maintained manually, it can be tedious and
error prone to store their versions in the `dependencies` array. Therefore, it's often better to use
a `version_extract` field instead. When set, this map will trigger a fetch of the version
directly from a file in the repository of the project.

Example:

```yaml
categories:
  - product

dependencies:
  - name: "bitnami/jruby"
    version_extract:
      file_path: Dockerfile
      regexp: '(?m)^\s+image: "bitnami\/jruby:(.*)"'
      ref: master
    package_manager: "docker"
    dependency_file_path: "Dockerfile:18"
    path_with_namespace: "gitlab-org/containers/bitnami/jruby"
```

In this example, the `version` of "bitnami/jruby" will be set during the [`sync`
phase](#sync_projects_data) with the value extracted thanks to the `regexp` and the content of
the file `Dockerfile`.

Note: `ref` is optional, and will default to the default branch of the repository if omitted.

### Folders

Gib is using these folders:

- `data`: contains all the projects metadata
- `db`: computed metadata in a single sqlite file
- `reports`: Generated reports

#### Files used by gib

The following files are used by `gib`:
- `ignore`: in a group folder, skip the sync of the group
- `properties.yml`: Used for categorization of projects
- `project.json`: Stored projects metadata (generated)
- `group.json`: Stored groups metadata (generated)
- `dependencies.json`: Stored projects dependencies (generated)
- `vulnerabilities.json`: Stored projects vulnerabilities (generated)
- `ci-config.json`: Stored projects ci-configuration (generated)
- `protected_branches.json`: Stored projects protected branches (generated)

#### Permitted files

- `README.md`
- `.gitkeep`
- `.DS_Store`

Any other file will be considered invalid. Feel free to contribute to this project if other files
need to be added.

