package main

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/google/go-cmp/cmp"
	log "github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
	"sigs.k8s.io/yaml"
)

func TestMergeDependencies(t *testing.T) {
	p := Project{
		Project: &gitlab.Project{
			Name:              "declarative-policy",
			DefaultBranch:     "main",
			PathWithNamespace: "gitlab-org/declarative-policy",
		},
		Dependencies: []Dependency{
			{
				Name:               "abstract_type",
				Version:            "0.0.7",
				PackageManager:     "bundler",
				DependencyFilePath: "Gemfile.lock",
			},
			{
				Name:               "activesupport",
				Version:            "6.0.3.4",
				PackageManager:     "bundler",
				DependencyFilePath: "Gemfile.lock",
			},
		},
	}
	log.SetOutput(ioutil.Discard)

	mux, ts := testServer(t)
	defer ts.Close()

	client, err := gitlab.NewClient("asdf123", gitlab.WithBaseURL(ts.URL))
	if err != nil {
		t.Fatal(err)
	}

	t.Run("Add new dependency", func(t *testing.T) {

		propertiesYAML :=
			`
categories:
  - product

dependencies:
  - name: "bitnami/jruby"
    version: "latest"
    package_manager: "docker"
    dependency_file_path: "Dockerfile:18"
    path_with_namespace: "gitlab-org/containers/bitnami/jruby"
`

		if err := yaml.Unmarshal([]byte(propertiesYAML), &p.Properties); err != nil {
			t.Fatal(err)
		}

		if err := p.mergeDependencies(client); err != nil {
			t.Fatal(err)
		}

		want := []Dependency{
			{
				Name:               "abstract_type",
				Version:            "0.0.7",
				PackageManager:     "bundler",
				DependencyFilePath: "Gemfile.lock",
			},
			{
				Name:               "activesupport",
				Version:            "6.0.3.4",
				PackageManager:     "bundler",
				DependencyFilePath: "Gemfile.lock",
			},
			{
				Name:               "bitnami/jruby",
				Version:            "latest",
				PackageManager:     "docker",
				DependencyFilePath: "Dockerfile:18",
				PathWithNamespace:  "gitlab-org/containers/bitnami/jruby",
			},
		}

		if diff := cmp.Diff(want, p.Dependencies); diff != "" {
			t.Errorf("Output mismatch (-want +got):\n%s", diff)
		}
	})

	t.Run("Version Extract", func(t *testing.T) {

		propertiesYAML :=
			`
categories:
  - product

dependencies:
  - name: "bandit"
    version_extract:
      file_path: docker/Dockerfile
      regexp: 'ENV SCANNER_VERSION \$\{SCANNER_VERSION:-(.*)\}'
    package_manager: "docker"
    dependency_file_path: "docker/Dockerfile:18"
    path_with_namespace: "gitlab-org/security-products/dependencies/bandit"
`

		mux.HandleFunc("/api/v4/projects/gitlab-org%2Fdeclarative-policy/repository/files/docker%2FDockerfile", func(w http.ResponseWriter, r *http.Request) {

			q := r.URL.Query()

			if diff := cmp.Diff(q["ref"], []string{"main"}); diff != "" {
				t.Errorf("ref mismatch (-want, +got): %s", diff)
			}
			content := `
FROM golang:1.17-alpine AS build
[...]

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION:-1.7.1}

[...]
`
			str := base64.StdEncoding.EncodeToString([]byte(content))

			fmt.Fprintln(w, `
				{
					"file_name": "Dockerfile",
					"file_path": "docker/Dockerfile",
					"size": 1476,
					"encoding": "base64",
					"content": "`+str+`",
					"content_sha256": "4c294617b60715c1d218e61164a3abd4808a4284cbc30e6728a01ad9aada4481",
					"ref": "main",
					"blob_id": "79f7bbd25901e8334750839545a9bd021f0e4c83",
					"commit_id": "d5a3ff139356ce33e37e73add446f16869741b50",
					"last_commit_id": "570e7b2abdd848b95f2f578043fc23bd6f6fd24d"
			  }`)

		}).Methods(http.MethodGet)

		if err := yaml.Unmarshal([]byte(propertiesYAML), &p.Properties); err != nil {
			t.Fatal(err)
		}

		want := Dependency{
			Name:               "bandit",
			Version:            "1.7.1",
			PackageManager:     "docker",
			DependencyFilePath: "docker/Dockerfile:18",
			PathWithNamespace:  "gitlab-org/security-products/dependencies/bandit",
		}

		if err := p.mergeDependencies(client); err != nil {
			t.Fatal(err)
		}

		if diff := cmp.Diff(want, p.Dependencies[len(p.Dependencies)-1]); diff != "" {
			t.Errorf("Output mismatch (-want +got):\n%s", diff)
			fmt.Printf("p.Properties.Dependencies = %+v\n", p.Properties.Dependencies)
		}

		t.Run("Invalid regex", func(t *testing.T) {

			propertiesYAML :=
				`
categories:
  - product

dependencies:
  - name: "bandit"
    version_extract:
      file_path: docker/Dockerfile
      regexp: '['
    package_manager: "docker"
    dependency_file_path: "docker/Dockerfile:18"
    path_with_namespace: "gitlab-org/security-products/dependencies/bandit"
`
			if err := yaml.Unmarshal([]byte(propertiesYAML), &p.Properties); err != nil {
				t.Fatal(err)
			}
			if err := p.mergeDependencies(&gitlab.Client{}); err == nil {
				t.Error("mergeDependencies should have raised an error")
			}
		})

		t.Run("No match", func(t *testing.T) {

			propertiesYAML :=
				`
categories:
  - product

dependencies:
  - name: "bandit"
    version_extract:
      file_path: docker/Dockerfile
      regexp: 'will not match'
    package_manager: "docker"
    dependency_file_path: "docker/Dockerfile:18"
    path_with_namespace: "gitlab-org/security-products/dependencies/bandit"
`
			if err := yaml.Unmarshal([]byte(propertiesYAML), &p.Properties); err != nil {
				t.Fatal(err)
			}
			if err := p.mergeDependencies(client); err == nil {
				t.Error("mergeDependencies should have raised an error")
			}
		})

		t.Run("File not found", func(t *testing.T) {

			propertiesYAML :=
				`
categories:
  - product

dependencies:
  - name: "bandit"
    version_extract:
      file_path: non_existing_file
      regexp: 'ENV SCANNER_VERSION \$\{SCANNER_VERSION:-(.*)\}'
    package_manager: "docker"
    dependency_file_path: "docker/Dockerfile:18"
    path_with_namespace: "gitlab-org/security-products/dependencies/bandit"
`
			if err := yaml.Unmarshal([]byte(propertiesYAML), &p.Properties); err != nil {
				t.Fatal(err)
			}
			if err := p.mergeDependencies(client); err == nil {
				t.Error("mergeDependencies should have raised an error")
			}
		})
	})
}
