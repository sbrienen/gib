#!/bin/bash
# strict mode - http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
IFS=$'\n\t'

if [ -z "$GITLAB_API_TOKEN" ]; then
  echo "WARNING: GITLAB_API_TOKEN is not set, reports generation might fail"
fi

DATADIR=${DATA_DIR_PATH:=./data}
INVENTORY_DB=$DATADIR/../db/inventory.db
TEMP_FOLDER=tmp/
INVENTORY_SITE=inventory
CI_DEFAULT_BRANCH=${CI_DEFAULT_BRANCH:=main}

if [ -f "${INVENTORY_DB}.7z" ]; then
  echo "Extracting inventory DB"
  7z x -y ${INVENTORY_DB}.7z
fi

echo "Configuring your inventory website"
# the temporary folder is an artifact, and should have been created already
mkdir -p $TEMP_FOLDER
cd $TEMP_FOLDER

if [ -d "${INVENTORY_SITE}" ]; then
  echo "${INVENTORY_SITE} already exists, skipping creation"
else
  hugo new site inventory
  ln -sf /usr/local/share/themes/gitlab ${INVENTORY_SITE}/themes/
  cp -r ${INVENTORY_SITE}/themes/gitlab/config/ ${INVENTORY_SITE}/
  cp ${INVENTORY_SITE}/themes/gitlab/config/_default/config.toml ${INVENTORY_SITE}/

  echo "Adding SSL reports to static/ and data/urls/"
  mkdir ${INVENTORY_SITE}/data/urls/
  cp -a ssl ${INVENTORY_SITE}/static/
  mv ${INVENTORY_SITE}/static/ssl/*.json ${INVENTORY_SITE}/data/urls || true

  echo "Linking inventory data to your site data"
  ln -sf $PWD/../$DATADIR ${INVENTORY_SITE}/data/projects

  echo "Copying violations.json to data"
  cp violations.json ${INVENTORY_SITE}/data/
fi

# Some tables might not have been created, hence the test for their existance
tables=$(sqlite-utils tables ../$INVENTORY_DB --csv --no-headers)

echo "Generating projects pages..."
# Extract projects from the local DB to create Hugo pages (`content` directory)

if $(echo $tables | grep -q "urls"); then
  request=$(
    cat <<-END
    select projects.id, projects.path_with_namespace, json_group_array(categories.category) as categories, json_group_array(distinct(urls.url)) as urls
    from projects
      left outer join categories on categories.project_id = projects.id
      left outer join urls on urls.project_id = projects.id
    group by categories.project_id, projects.path_with_namespace
END
  )
else
  request=$(
    cat <<-END
    select projects.id, projects.path_with_namespace, json_group_array(categories.category) as categories
    from projects
      left outer join categories on categories.project_id = projects.id
    group by categories.project_id, projects.path_with_namespace
END
  )
fi

sqlite-utils ../$INVENTORY_DB "$request" --nl --json-cols |
  while IFS= read -r line; do
    path_with_namespace=$(echo $line | jq .path_with_namespace -r)
    dir=${INVENTORY_SITE}/content/projects/$(dirname $path_with_namespace)
    project=$(basename $path_with_namespace)
    mkdir -p $dir
    echo Creating $path_with_namespace
    echo $line | jq 'del(.categories,.urls | select(. == [null]))' >${dir}/${project}.md
  done

echo "Generating projects metric..."
projects_metrics=${INVENTORY_SITE}/data/projects_metrics.json
sqlite-utils ../$INVENTORY_DB "select sum(1) as total_number_of_projects from projects" --nl >$projects_metrics

git diff --name-only --diff-filter=A "$(git rev-list -1 --before 7.days.ago origin/$CI_DEFAULT_BRANCH)" origin/$CI_DEFAULT_BRANCH | grep -e 'project.json$' | sed -e 's/^data//g' -e 's/\/project.json//g' | jq -ncMR '{"added": [inputs]}' | jq -s add $projects_metrics - >$projects_metrics.tmp && mv $projects_metrics.tmp $projects_metrics
git diff --name-only --diff-filter=D "$(git rev-list -1 --before 7.days.ago origin/$CI_DEFAULT_BRANCH)" origin/$CI_DEFAULT_BRANCH | grep -e 'project.json$' | sed -e 's/^data//g' -e 's/\/project.json//g' | jq -ncMR '{"deleted": [inputs]}' | jq -s add $projects_metrics - >$projects_metrics.tmp && mv $projects_metrics.tmp $projects_metrics
git diff --name-only --diff-filter=MRT "$(git rev-list -1 --before 7.days.ago origin/$CI_DEFAULT_BRANCH)" origin/$CI_DEFAULT_BRANCH | grep -e 'project.json$' | sed -e 's/^data//g' -e 's/\/project.json//g' | jq -ncMR '{"updated": [inputs]}' | jq -s add $projects_metrics - >$projects_metrics.tmp && mv $projects_metrics.tmp $projects_metrics
# Count ignored groups
find ../$DATADIR -name ignore -type f | wc -l | awk '{print $1}' | jq -ncMR '{"ignored": inputs}' | jq -s add $projects_metrics - >$projects_metrics.tmp && mv $projects_metrics.tmp $projects_metrics

# cleanup
rm -f $projects_metrics.tmp

if $(echo $tables | grep -q "vulnerabilities"); then
  echo "Generating Vulnerabilities data"
  mkdir -p ${INVENTORY_SITE}/data/vulnerabilities
  request=$(
    cat <<-END
  select severity,
          sum(case when created_at <  DATE('now', '-30 day') then 1 else 0 end) as over_30_days,
          sum(case when created_at <  DATE('now', '-60 day') then 1 else 0 end) as over_60_days,
          sum(case when created_at <  DATE('now', '-90 day') then 1 else 0 end) as over_90_days,
          sum(case when created_at <  DATE('now', '-120 day') then 1 else 0 end) as over_120_days,
          sum(case when created_at <  DATE('now', '-1 year') then 1 else 0 end) as over_1_year,
          sum(1) all_time
  from vulnerabilities
  where state in ('detected', 'confirmed')
  group by 1
  order by case
          when severity='critical' then 1
          when severity='high' then 2
          when severity='medium' then 3
          when severity='low' then 4
          when severity='info' then 5
          when severity='unknown' then 6
  end
END
  )
  sqlite-utils ../$INVENTORY_DB "$request" >${INVENTORY_SITE}/data/vulnerabilities/aging.json

  request=$(
    cat <<-END
   SELECT projects.path_with_namespace,
    CAST( AVG(CASE WHEN  severity IN ('critical','high','unknown') THEN julianday('now') - julianday(vulnerabilities.created_at) END) as INTEGER) as avg_age_of_vulnerabilities_crit,
    sum(CASE WHEN severity IN ('critical','high','unknown') THEN 1 END) as number_of_vulnerabilities_crit,
    CAST( AVG(julianday('now') - julianday(vulnerabilities.created_at)) as INTEGER) as avg_age_of_vulnerabilities,
    SUM(1) as number_of_vulnerabilities
   FROM vulnerabilities
   JOIN projects ON vulnerabilities.project_id = projects.id
   WHERE state IN ('detected','confirmed')
   GROUP by 1
   ORDER BY 2 DESC
   LIMIT 10
END
  )
  sqlite-utils ../$INVENTORY_DB "$request" >${INVENTORY_SITE}/data/vulnerabilities/oldest_average.json

  request=$(
    cat <<-END
  SELECT
    PROJECTS.PATH_WITH_NAMESPACE,
    COALESCE(SUM(CASE WHEN STATE = 'dismissed' THEN 1 END),0) AS FP,
    COALESCE(SUM(CASE WHEN STATE IN ('confirmed', 'resolved') THEN 1 END), 0) AS TP,
    SUM(1) as total,
    ROUND(COALESCE(SUM(CASE WHEN STATE = 'dismissed' THEN 1 END)/CAST(SUM(1) AS FLOAT),0)*100, 2) AS FPR,
    ROUND(COALESCE(SUM(CASE WHEN STATE IN ('confirmed', 'resolved') THEN 1 END)/CAST(SUM(1) AS FLOAT),0)*100,2) AS TPR
  FROM VULNERABILITIES
    JOIN PROJECTS ON VULNERABILITIES.PROJECT_ID = PROJECTS.ID
  WHERE STATE IN ('dismissed', 'confirmed', 'resolved')
  GROUP BY 1
  ORDER BY FPR DESC, TOTAL DESC
  LIMIT 10;
END
  )
  sqlite-utils ../$INVENTORY_DB "$request" >${INVENTORY_SITE}/data/vulnerabilities/worse_false_positive_rates.json

  request=$(
    cat <<-END
  SELECT
    PROJECTS.PATH_WITH_NAMESPACE,
    COALESCE(SUM(CASE WHEN STATE = 'dismissed' THEN 1 END),0) AS FP,
    COALESCE(SUM(CASE WHEN STATE IN ('confirmed', 'resolved') THEN 1 END), 0) AS TP,
    SUM(1) as total,
    ROUND(COALESCE(SUM(CASE WHEN STATE = 'dismissed' THEN 1 END)/CAST(SUM(1) AS FLOAT),0)*100, 2) AS FPR,
    ROUND(COALESCE(SUM(CASE WHEN STATE IN ('confirmed', 'resolved') THEN 1 END)/CAST(SUM(1) AS FLOAT),0)*100,2) AS TPR
  FROM VULNERABILITIES
    JOIN PROJECTS ON VULNERABILITIES.PROJECT_ID = PROJECTS.ID
  WHERE STATE IN ('dismissed', 'confirmed', 'resolved')
  GROUP BY 1
  ORDER BY TPR DESC, TOTAL DESC
  LIMIT 10;
END
  )
  sqlite-utils ../$INVENTORY_DB "$request" >${INVENTORY_SITE}/data/vulnerabilities/best_true_positive_rates.json

  request=$(
    cat <<-END
  select
    projects.path_with_namespace,

    coalesce(sum(case when updated_at >= date('now', 'weekday 0', '-37 days') and state='confirmed' and severity = 'critical' then 1 end), 0) confirmed_critical,
    coalesce(sum(case when updated_at >= date('now', 'weekday 0', '-37 days') and state='confirmed' and severity = 'high' then 1 end), 0) confirmed_high,
    coalesce(sum(case when updated_at >= date('now', 'weekday 0', '-37 days') and state='confirmed' and severity = 'unknown' then 1 end), 0) confirmed_unknown,
    coalesce(sum(case when updated_at >= date('now', 'weekday 0', '-37 days') and state='confirmed' and severity = 'medium' then 1 end), 0) confirmed_medium,
    coalesce(sum(case when updated_at >= date('now', 'weekday 0', '-37 days') and state='confirmed' and severity = 'low' then 1 end), 0) confirmed_low,
    coalesce(sum(case when updated_at >= date('now', 'weekday 0', '-37 days') and state='confirmed' and severity = 'info' then 1 end), 0) confirmed_info,
    coalesce(sum(case when updated_at >= date('now', 'weekday 0', '-37 days') and state='confirmed' then 1 end), 0) total_confirmed,

    coalesce(sum(case when dismissed_at >= date('now', 'weekday 0', '-37 days') and state='dismissed' and severity = 'critical' then 1 end), 0) dismissed_critical,
    coalesce(sum(case when dismissed_at >= date('now', 'weekday 0', '-37 days') and state='dismissed' and severity = 'high' then 1 end), 0) dismissed_high,
    coalesce(sum(case when dismissed_at >= date('now', 'weekday 0', '-37 days') and state='dismissed' and severity = 'unknown' then 1 end), 0) dismissed_unknown,
    coalesce(sum(case when dismissed_at >= date('now', 'weekday 0', '-37 days') and state='dismissed' and severity = 'medium' then 1 end), 0) dismissed_medium,
    coalesce(sum(case when dismissed_at >= date('now', 'weekday 0', '-37 days') and state='dismissed' and severity = 'low' then 1 end), 0) dismissed_low,
    coalesce(sum(case when dismissed_at >= date('now', 'weekday 0', '-37 days') and state='dismissed' and severity = 'info' then 1 end), 0) dismissed_info,
    coalesce(sum(case when dismissed_at >= date('now', 'weekday 0', '-37 days') and state='dismissed' then 1 end), 0) total_dismissed,

    coalesce(sum(case when resolved_at >= date('now', 'weekday 0', '-37 days') and state='resolved' and severity = 'critical' then 1 end), 0) resolved_critical,
    coalesce(sum(case when resolved_at >= date('now', 'weekday 0', '-37 days') and state='resolved' and severity = 'high' then 1 end), 0) resolved_high,
    coalesce(sum(case when resolved_at >= date('now', 'weekday 0', '-37 days') and state='resolved' and severity = 'unknown' then 1 end), 0) resolved_unknown,
    coalesce(sum(case when resolved_at >= date('now', 'weekday 0', '-37 days') and state='resolved' and severity = 'medium' then 1 end), 0) resolved_medium,
    coalesce(sum(case when resolved_at >= date('now', 'weekday 0', '-37 days') and state='resolved' and severity = 'low' then 1 end), 0) resolved_low,
    coalesce(sum(case when resolved_at >= date('now', 'weekday 0', '-37 days') and state='resolved' and severity = 'info' then 1 end), 0) resolved_info,
    coalesce(sum(case when resolved_at >= date('now', 'weekday 0', '-37 days') and state='resolved' then 1 end),0) total_resolved

  from vulnerabilities
    join projects on vulnerabilities.project_id = projects.id
  where state in ('dismissed', 'resolved', 'confirmed')
    and (dismissed_at >= date('now', 'weekday 0', '-37 days')
    or resolved_at >= date('now', 'weekday 0', '-37 days')
    or updated_at >= date('now', 'weekday 0', '-37 days')
    )
  group by 1
  order by total_dismissed DESC , total_resolved DESC
END
  )
  sqlite-utils ../$INVENTORY_DB "$request" >${INVENTORY_SITE}/data/vulnerabilities/triage_ratios.json

  request=$(
    cat <<-END
    with dates as (
      select datetime('now', '-6 months') as reportWeek
      union all
      select datetime(reportWeek,  '+7 days')
      from dates
      where reportWeek < datetime('now', 'weekday 0', '-7 days')
     ), weekly as (
    select strftime('%W', case state when 'dismissed' then dismissed_at when 'confirmed' then updated_at when 'resolved' then resolved_at end) WeekNumber,
      sum(case when severity = 'critical' then 1 else 0 end) critical,
      sum(case when severity = 'critical' and state = 'dismissed' then 1 else 0 end) dismissed_critical,
      sum(case when severity = 'critical' and state = 'confirmed' then 1 else 0 end) confirmed_critical,
      sum(case when severity = 'critical' and state = 'resolved' then 1 else 0 end) resolved_critical,
      sum(case when severity = 'high' then 1 else 0 end) high,
      sum(case when severity = 'high' and state = 'dismissed' then 1 else 0 end) dismissed_high,
      sum(case when severity = 'high' and state = 'confirmed' then 1 else 0 end) confirmed_high,
      sum(case when severity = 'high' and state = 'resolved' then 1 else 0 end) resolved_high,
      sum(case when severity = 'unknown' then 1 else 0 end) unknown,
      sum(case when severity = 'unknown' and state = 'dismissed' then 1 else 0 end) dismissed_unknown,
      sum(case when severity = 'unknown' and state = 'confirmed' then 1 else 0 end) confirmed_unknown,
      sum(case when severity = 'unknown' and state = 'resolved' then 1 else 0 end) resolved_unknown,
      sum(case when severity = 'medium' then 1 else 0 end) medium,
      sum(case when severity = 'medium' and state = 'dismissed' then 1 else 0 end) dismissed_medium,
      sum(case when severity = 'medium' and state = 'confirmed' then 1 else 0 end) confirmed_medium,
      sum(case when severity = 'medium' and state = 'resolved' then 1 else 0 end) resolved_medium,
      sum(case when severity = 'low' then 1 else 0 end) low,
      sum(case when severity = 'low' and state = 'dismissed' then 1 else 0 end) dismissed_low,
      sum(case when severity = 'low' and state = 'confirmed' then 1 else 0 end) confirmed_low,
      sum(case when severity = 'low' and state = 'resolved' then 1 else 0 end) resolved_low,
      sum(case when severity = 'info' then 1 else 0 end) info,
      sum(case when severity = 'info' and state = 'dismissed' then 1 else 0 end) dismissed_info,
      sum(case when severity = 'info' and state = 'confirmed' then 1 else 0 end) confirmed_info,
      sum(case when severity = 'info' and state = 'resolved' then 1 else 0 end) resolved_info,
      sum(1) total
    from vulnerabilities
    where
    (state = 'dismissed' and dismissed_at >= datetime('now', '-6 months'))
    or (state = 'confirmed' and updated_at >= datetime('now', '-6 months'))
    or (state = 'resolved' and resolved_at >= datetime('now', '-6 months'))
    group by 1
    order by 2
    )
    select
      strftime('%W', dates.reportWeek) WeekNumber,
      date(dates.reportWeek, 'weekday 0', '-6 day') WeekStart,
      date(dates.reportWeek, 'weekday 0') WeekEnd,
      coalesce(critical,0) critical,
      coalesce(dismissed_critical,0) dismissed_critical,
      coalesce(confirmed_critical,0) confirmed_critical,
      coalesce(resolved_critical,0) resolved_critical,
      coalesce(high,0) high,
      coalesce(dismissed_high,0) dismissed_high,
      coalesce(confirmed_high,0) confirmed_high,
      coalesce(resolved_high,0) resolved_high,
      coalesce(unknown,0) unknown,
      coalesce(dismissed_unknown,0) dismissed_unknown,
      coalesce(confirmed_unknown,0) confirmed_unknown,
      coalesce(resolved_unknown,0) resolved_unknown,
      coalesce(medium,0) medium,
      coalesce(dismissed_medium,0) dismissed_medium,
      coalesce(confirmed_medium,0) confirmed_medium,
      coalesce(resolved_medium,0) resolved_medium,
      coalesce(low,0) low,
      coalesce(dismissed_low,0) dismissed_low,
      coalesce(confirmed_low,0) confirmed_low,
      coalesce(resolved_low,0) resolved_low,
      coalesce(info,0) info,
      coalesce(dismissed_info,0) dismissed_info,
      coalesce(confirmed_info,0) confirmed_info,
      coalesce(resolved_info,0) resolved_info,
      coalesce(total,0) total
    from dates left outer join weekly on  strftime('%W', dates.reportWeek) = weekly.WeekNumber
END
  )
  sqlite-utils ../$INVENTORY_DB "$request" >${INVENTORY_SITE}/data/vulnerabilities/velocity_by_week.json

  request=$(
    cat <<-END
    with dates as (
      select datetime('now', '-6 months') as month
      union all
      select datetime(month,  '+1 month')
      from dates
      where month < datetime('now', 'weekday 0', '-7 days')
     ), monthly as (
    select strftime('%m', case state when 'dismissed' then dismissed_at when 'confirmed' then updated_at when 'resolved' then resolved_at end) month,
      sum(case when severity = 'critical' then 1 else 0 end) critical,
      sum(case when severity = 'critical' and state = 'dismissed' then 1 else 0 end) dismissed_critical,
      sum(case when severity = 'critical' and state = 'confirmed' then 1 else 0 end) confirmed_critical,
      sum(case when severity = 'critical' and state = 'resolved' then 1 else 0 end) resolved_critical,
      sum(case when severity = 'high' then 1 else 0 end) high,
      sum(case when severity = 'high' and state = 'dismissed' then 1 else 0 end) dismissed_high,
      sum(case when severity = 'high' and state = 'confirmed' then 1 else 0 end) confirmed_high,
      sum(case when severity = 'high' and state = 'resolved' then 1 else 0 end) resolved_high,
      sum(case when severity = 'unknown' then 1 else 0 end) unknown,
      sum(case when severity = 'unknown' and state = 'dismissed' then 1 else 0 end) dismissed_unknown,
      sum(case when severity = 'unknown' and state = 'confirmed' then 1 else 0 end) confirmed_unknown,
      sum(case when severity = 'unknown' and state = 'resolved' then 1 else 0 end) resolved_unknown,
      sum(case when severity = 'medium' then 1 else 0 end) medium,
      sum(case when severity = 'medium' and state = 'dismissed' then 1 else 0 end) dismissed_medium,
      sum(case when severity = 'medium' and state = 'confirmed' then 1 else 0 end) confirmed_medium,
      sum(case when severity = 'medium' and state = 'resolved' then 1 else 0 end) resolved_medium,
      sum(case when severity = 'low' then 1 else 0 end) low,
      sum(case when severity = 'low' and state = 'dismissed' then 1 else 0 end) dismissed_low,
      sum(case when severity = 'low' and state = 'confirmed' then 1 else 0 end) confirmed_low,
      sum(case when severity = 'low' and state = 'resolved' then 1 else 0 end) resolved_low,
      sum(case when severity = 'info' then 1 else 0 end) info,
      sum(case when severity = 'info' and state = 'dismissed' then 1 else 0 end) dismissed_info,
      sum(case when severity = 'info' and state = 'confirmed' then 1 else 0 end) confirmed_info,
      sum(case when severity = 'info' and state = 'resolved' then 1 else 0 end) resolved_info,
      sum(1) total
    from vulnerabilities
    where
      (state = 'dismissed' and dismissed_at >= datetime('now', '-6 months'))
      or (state = 'confirmed' and updated_at >= datetime('now', '-6 months'))
      or (state = 'resolved' and resolved_at >= datetime('now', '-6 months'))
    group by 1
    order by 2
    )
    select
      strftime('%Y-%m', dates.month) month,
      coalesce(critical,0) critical,
      coalesce(dismissed_critical,0) dismissed_critical,
      coalesce(confirmed_critical,0) confirmed_critical,
      coalesce(resolved_critical,0) resolved_critical,
      coalesce(high,0) high,
      coalesce(dismissed_high,0) dismissed_high,
      coalesce(confirmed_high,0) confirmed_high,
      coalesce(resolved_high,0) resolved_high,
      coalesce(unknown,0) unknown,
      coalesce(dismissed_unknown,0) dismissed_unknown,
      coalesce(confirmed_unknown,0) confirmed_unknown,
      coalesce(resolved_unknown,0) resolved_unknown,
      coalesce(medium,0) medium,
      coalesce(dismissed_medium,0) dismissed_medium,
      coalesce(confirmed_medium,0) confirmed_medium,
      coalesce(resolved_medium,0) resolved_medium,
      coalesce(low,0) low,
      coalesce(dismissed_low,0) dismissed_low,
      coalesce(confirmed_low,0) confirmed_low,
      coalesce(resolved_low,0) resolved_low,
      coalesce(info,0) info,
      coalesce(dismissed_info,0) dismissed_info,
      coalesce(confirmed_info,0) confirmed_info,
      coalesce(resolved_info,0) resolved_info,
      coalesce(total,0) total
    from dates left outer join monthly on  strftime('%m', dates.month) = monthly.month
END
  )
  sqlite-utils ../$INVENTORY_DB "$request" >${INVENTORY_SITE}/data/vulnerabilities/velocity_by_month.json

  # Breakdown (Open vulnerabilities)
  request=$(
    cat <<-END
    select projects.path_with_namespace, sum(case when severity in ('critical', 'high', 'unknown') then 1 else 0 end) critical_high_unknown,  sum(case when severity not in ('critical', 'high', 'unknown') then 1 else 0 end) others, sum(1) total
    from vulnerabilities
      join projects on vulnerabilities.project_id = projects.id
    where state in ('detected','confirmed')
    group by 1
    order by total DESC
END
  )
  sqlite-utils ../$INVENTORY_DB "$request" >${INVENTORY_SITE}/data/vulnerabilities/breakdown.json

  # Breakdown (False postives)
  request=$(
    cat <<-END
    select projects.path_with_namespace, sum(case when severity in ('critical', 'high', 'unknown') then 1 else 0 end) critical_high_unknown,  sum(case when severity not in ('critical', 'high', 'unknown') then 1 else 0 end) others, sum(1) total
    from vulnerabilities
      join projects on vulnerabilities.project_id = projects.id
    where state in ('dismissed')
    group by 1
    order by total DESC
END
  )
  sqlite-utils ../$INVENTORY_DB "$request" >${INVENTORY_SITE}/data/vulnerabilities/breakdown_fp.json

fi

# Generate site
hugo -s ${INVENTORY_SITE} -d ../../public

if [ -n "$HUGO_BASEURL" ]; then
  echo "Your inventory reports are ready and will be available at: $HUGO_BASEURL"
fi
