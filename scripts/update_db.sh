#!/bin/bash
# strict mode - http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
IFS=$'\n\t'

DEPENDENCIES="dependencies.json"
VULNERABILITIES="vulnerabilities.json"
CI_CONFIG="ci-config.json"
PROJECTS_VIOLATIONS="violations.json"
DATADIR=${DATA_DIR_PATH:=./data}
INVENTORY_DB=./db/inventory.db
SSL_REPORTS_PATH=${SSL_REPORTS_PATH:=tmp/ssl}

if [ "$(find "${DATADIR}" -maxdepth 0 -empty | wc -c)" -gt 0 ]; then
  echo "${DATADIR} is empty."
  exit 0
fi

# init
mkdir -p $DATADIR/../db
echo -n "Using database: $(realpath $INVENTORY_DB)"
# Reset DB
true >$INVENTORY_DB

# variables for foreign keys are set when at least one record, so the table is created
CATEGORIES_FK=""
URLS_FK=""
DEPENDENCIES_FK=""
VULNERABILITIES_FK=""
CI_CONFIG_FK=""
PROJECTS_VIOLATIONS_FK=""
URL_VIOLATIONS_FK=""

# store projects
find "$DATADIR" -type f -name project.json -print0 |
  while IFS= read -r -d '' project_json; do
    PROJECT=$(dirname "$project_json")
    echo -ne "\n[$PROJECT] Storing: "
    echo -n "Project"
    sqlite-utils insert $INVENTORY_DB projects "$project_json" --pk=id --replace --alter

    if [[ -f "$PROJECT/properties.yml" ]]; then
      # Store categories
      echo -n ", Categories"
      yq e '.categories' -o=json "$PROJECT/properties.yml" | jq --argjson projectMeta "$(<"$project_json")" '[.] | transpose | map({ category:.[0], project_id: $projectMeta.id })' | sqlite-utils insert $INVENTORY_DB categories --pk=category --pk=project_id --replace --alter -
      CATEGORIES_FK="categories project_id projects id"

      # Store URLs
      if [[ "$(yq e '.urls | length' "$PROJECT/properties.yml")" -gt "0" ]]; then
        echo -n ", URLs"
        yq e '.urls' -o=json "$PROJECT/properties.yml" | jq --argjson projectMeta "$(<"$project_json")" '[.] | transpose | map({ url:.[0], project_id: $projectMeta.id })' | sqlite-utils insert $INVENTORY_DB urls --pk=url --pk=project_id --replace --alter -
        URLS_FK="urls project_id projects id"
      fi
    fi

    if [[ -f "$PROJECT/$DEPENDENCIES" ]]; then
      echo -n ", Dependencies"
      jq --argjson projectMeta "$(<"$project_json")" '.[].project_id += $projectMeta.id' "$PROJECT/$DEPENDENCIES" | sqlite-utils insert $INVENTORY_DB dependencies --pk=name --pk=version --pk=dependency_file_path --pk=project_id --replace --alter -
      DEPENDENCIES_FK="dependencies project_id projects id"
    fi

    if [[ -f "$PROJECT/$VULNERABILITIES" ]]; then
      echo -n ", Vulnerabilities"
      sqlite-utils insert $INVENTORY_DB vulnerabilities --pk=id --replace --alter --flatten $PROJECT/$VULNERABILITIES
      VULNERABILITIES_FK="vulnerabilities project_id projects id"
    fi

    if [[ -f "$PROJECT/$CI_CONFIG" ]]; then
      echo -n ", CI-configuration"
      jq --argjson projectMeta "$(<"$project_json")" '.project_id += $projectMeta.id' "$PROJECT/$CI_CONFIG" | sqlite-utils insert $INVENTORY_DB ci_configs --pk=project_id --replace --alter -
      CI_CONFIG_FK="ci_configs project_id projects id"
    fi

    if [[ -f "$PROJECT/$PROJECTS_VIOLATIONS" ]] && [[ "$(<"$PROJECT/$PROJECTS_VIOLATIONS")" != "{}" ]]; then
      echo -n ", Violations"
      jq --argjson projectMeta "$(<"$project_json")" '[.result[].expressions[].value] | map({ violation:.msg, description:.description, key:.key, project_id: $projectMeta.id })' "$PROJECT/$PROJECTS_VIOLATIONS" | sqlite-utils insert $INVENTORY_DB projects_violations --pk=key --pk=project_id --replace --alter -
      PROJECTS_VIOLATIONS_FK="projects_violations project_id projects id"
    fi

  done

# store groups
find "$DATADIR" -type f -name group.json -print0 |
  while IFS= read -r -d '' group_json; do
    GROUP=$(dirname "$group_json")
    echo -ne "\n[$GROUP] Storing: "
    echo -n "Group"
    sqlite-utils insert $INVENTORY_DB groups "$group_json" --pk=id --replace --alter
  done

if [ -d "$SSL_REPORTS_PATH" ]; then
  # store URLs violations
  find $SSL_REPORTS_PATH -type f -name '*.violations.json' -print0 |
    while IFS= read -r -d '' violations_json; do
      if [[ "$(<"$violations_json")" != "{}" ]]; then
        URL=$(echo "https://$(basename $violations_json .violations.json)")
        echo -ne "\n[$URL] Storing: "
        echo -n "URL violation"
        jq --arg url "$URL" '[.result[].expressions[].value] | map({ violation:.msg, description:.description, key:.key, url: $url })' $violations_json | sqlite-utils insert $INVENTORY_DB urls_violations --pk=url --pk=key --replace --alter -
        URL_VIOLATIONS_FK="urls_violations url urls url"
      fi
    done
fi

# Ensure every foreign key has a corresponding index
sqlite-utils add-foreign-keys $INVENTORY_DB \
  $CATEGORIES_FK \
  $URLS_FK \
  $DEPENDENCIES_FK \
  $VULNERABILITIES_FK \
  $CI_CONFIG_FK \
  $PROJECTS_VIOLATIONS_FK \
  $URL_VIOLATIONS_FK
sqlite-utils index-foreign-keys $INVENTORY_DB

# Display table stats
echo -e "\n"
sqlite-utils tables $INVENTORY_DB --counts -t

echo "Compress DB..."
7z a $INVENTORY_DB.7z db/inventory.db
