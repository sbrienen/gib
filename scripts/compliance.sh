#!/bin/bash
# strict mode - http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
IFS=$'\n\t'

export PROJECTS_POLICIES=${PROJECTS_POLICIES:="/usr/local/share/policies/projects/"}
export PROJECTS_QUERY=${PROJECTS_QUERY:="data.gitlab.projects.violation[x]"}

export SITES_POLICIES=${SITES_POLICIES:="/usr/local/share/policies/sites/"}
export SITES_QUERY=${SITES_QUERY:="data.gitlab.sites.violation[x]"}

export VULNERABILITIES_POLICIES=${VULNERABILITIES_POLICIES:="/usr/local/share/policies/vulnerabilities/"}
export VULNERABILITIES_QUERY=${VULNERABILITIES_QUERY:="data.gitlab.vulnerabilities.violation[x]"}

DATADIR=${DATA_DIR_PATH:=./data}
# if $1 is provided, use it as the directory to run policies instead of DATA_DIR_PATH
if [ $# -ge 1 ] && [ -n "$1" ]; then
  DATADIR=$1
fi
SSL_REPORTS_PATH=${SSL_REPORTS_PATH:=tmp/ssl}

# Use local policies if present
if [ -d ./policies/projects ]; then export PROJECTS_POLICIES="./policies/projects"; fi
if [ -d ./policies/vulnerabilities ]; then export VULNERABILITIES_POLICIES="./policies/vulnerabilities"; fi
if [ -d ./policies/sites ]; then export SITES_POLICIES="./policies/sites"; fi

# Check paths
if [ ! -d "$PROJECTS_POLICIES" ]; then
  echo "PROJECTS_POLICIES ($PROJECTS_POLICIES) is not a directory"
  exit 1
fi
if [ ! -d "$VULNERABILITIES_POLICIES" ]; then
  echo "VULNERABILITIES_POLICIES ($VULNERABILITIES_POLICIES) is not a directory"
  exit 1
fi
if [ ! -d "$SITES_POLICIES" ]; then
  echo "SITES_POLICIES ($SITES_POLICIES) is not a directory"
  exit 1
fi

# build_input concatenates all data from projects into a single JSON input for OPA
build_input() {
  local dir=$1
  {
    jq '{"project": .}' $dir/project.json
    [ -f $dir/properties.yml ] && yq e -o=j $dir/properties.yml
    [ -f $dir/ci-config.json ] && jq -r '.merged_yaml' $dir/ci-config.json | yq e -o=j '{"ci-config": .}' -
    [ -f $dir/protected_branches.json ] && jq '{"protected_branches": .}' $dir/protected_branches.json
  } | jq -s add
}
# The function needs to be exported to the subshell we use in find below
export -f build_input

echo "Running Projects Compliance on $DATADIR:"
find $DATADIR -name project.json -exec bash -c '\
  echo "[$(dirname {})]" ; \
  build_input $(dirname {}) | \
  opa eval -I -d "$PROJECTS_POLICIES" "$PROJECTS_QUERY" | \
  tee $(dirname {})/violations.json | \
  jq "select(length > 0) | .result[].bindings[].msg" | \
  while read line; do echo -e "\033[0;31m${line}\033[0m"; done' \;

echo "Running Vulnerabilities Compliance on $DATADIR:"
find $DATADIR -name vulnerabilities.json -exec bash -c '\
  echo "[$(dirname {})]"
  opa eval -i {} -d "$VULNERABILITIES_POLICIES" "$VULNERABILITIES_QUERY" | \
  tee $(dirname {})/vulnerabilities-violations.json | \
  jq "select(length > 0) | .result[].bindings[].msg" | \
  while read line; do echo -e "\033[0;31m${line}\033[0m"; done; \
  jq -s ".[0].result + .[1].result | {result: .}  | del(..|nulls)" $(dirname {})/vulnerabilities-violations.json $(dirname {})/violations.json >$(dirname {})/merged-violations.json
  rm $(dirname {})/vulnerabilities-violations.json
  mv -f $(dirname {})/merged-violations.json $(dirname {})/violations.json' \;

if [ -d "$SSL_REPORTS_PATH" ]; then
  echo "Running Sites Compliance on $SSL_REPORTS_PATH:"
  find "$SSL_REPORTS_PATH" -name '*.json' -not -name '*.violations.json' -exec bash -c '
    echo "[$(basename {})]"; \
    opa eval -i {} -d "$SITES_POLICIES" "$SITES_QUERY" | \
    tee $(dirname {})/$(basename {} .json).violations.json | \
    jq "select(length > 0) | .result[].bindings[].msg" | \
    while read line; do echo -e "\033[0;31m${line}\033[0m"; done' \;
else
  echo "$SSL_REPORTS_PATH does not exist, skipping site compliance"
fi
