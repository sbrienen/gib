#!/bin/bash
# strict mode - http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
IFS=$'\n\t'

DATADIR=${DATA_DIR_PATH:=./data}
INVENTORY_DB=./db/inventory.db
TEMP_FOLDER=tmp

if [ -f "${INVENTORY_DB}.7z" ] && [ ! -f "${INVENTORY_DB}" ]; then
  echo "Extracting inventory DB"
  7z x -y ${INVENTORY_DB}.7z
fi

# Some tables might not have been created, hence the test for their existance
tables=$(sqlite-utils tables $INVENTORY_DB --csv --no-headers)

echo "Exporting violations from $INVENTORY_DB to ${TEMP_FOLDER}/violations.json"
if $(echo $tables | grep -q "urls_violations"); then
  request=$(
    cat <<-END
    select urls.project_id, projects.path_with_namespace, urls.url || ': ' || violations.violation as message, violations.description, violations.key
    from urls_violations as violations
      join urls on violations.url = urls.url
      join projects on urls.project_id = projects.id
END
  )
  sqlite-utils $INVENTORY_DB "$request" >${TEMP_FOLDER}/violations.json
else
  echo '[]'
fi >${TEMP_FOLDER}/violations.json

if $(echo $tables | grep -q "projects_violations"); then
  request=$(
    cat <<-END
    select violations.project_id, projects.path_with_namespace, violations.violation as message, violations.description, violations.key
    from projects_violations as violations
      join projects on violations.project_id = projects.id
END
  )
  sqlite-utils $INVENTORY_DB "$request" | jq -s add - ${TEMP_FOLDER}/violations.json >${TEMP_FOLDER}/violations.json.tmp && mv ${TEMP_FOLDER}/violations.json.tmp ${TEMP_FOLDER}/violations.json
fi

echo "$(jq 'length' ${TEMP_FOLDER}/violations.json) violation exported"
