package main

import (
	"bytes"
	_ "embed"
	"os/exec"

	"github.com/urfave/cli/v2"
)

//go:embed scripts/export_violations.sh
var exportViolationsScript []byte

func exportViolationsCmd(c *cli.Context) error {
	cmd := exec.Command("/bin/bash", "-s")
	cmd.Stdin = bytes.NewReader(exportViolationsScript)
	cmd.Stdout = c.App.Writer
	cmd.Stderr = c.App.Writer
	return cmd.Run()
}
