package main

import (
	"bytes"
	"fmt"
	"os"
	"path/filepath"
	"testing"

	"github.com/google/go-cmp/cmp"
	log "github.com/sirupsen/logrus"
)

func TestValidateCmd(t *testing.T) {
	// Create temp data-dir
	dataDir, err := os.MkdirTemp("", "datadir")
	if err != nil {
		log.Fatal(err)
	}
	defer os.RemoveAll(dataDir)

	// Configure dataDir with a main `gitlab-org` group, and existing projects
	err = os.MkdirAll(filepath.Join(dataDir, "gitlab-org", "advisories-community"), 0700)
	if err != nil {
		t.Fatal(err)
	}

	err = os.MkdirAll(filepath.Join(dataDir, "gitlab-org", "declarative-policy"), 0700)
	if err != nil {
		t.Fatal(err)
	}

	os.Setenv("DATA_DIR_PATH", dataDir)

	log.SetFormatter(&log.TextFormatter{
		DisableColors:    true,
		DisableTimestamp: true,
	})

	output := new(bytes.Buffer)
	app := NewApp()
	app.Writer = output

	t.Run("Correct output", func(t *testing.T) {
		err = app.Run([]string{os.Args[0], "validate"})
		if err != nil {
			t.Fatal(err)
		}
		got := output.String()

		want := fmt.Sprintf("level=info msg=\"Data directory clean (3 directories scanned)\" dataDir=%s\n", dataDir)

		if diff := cmp.Diff(want, got); diff != "" {
			// full output
			fmt.Printf("got = %+v\n", got)

			t.Errorf("Output mismatch (-want +got):\n%s", diff)
		}
	})

	t.Run("Only properties.yml files", func(t *testing.T) {
		unexpectedFile := filepath.Join(dataDir, "gitlab-org", "advisories-community", "result.json")
		err = os.WriteFile(unexpectedFile, []byte("{}"), 0600)
		if err != nil {
			t.Fatal(err)
		}
		defer os.Remove(unexpectedFile)

		// Clean-up our output buffer from previous test
		output.Reset()
		err = app.Run([]string{os.Args[0], "validate", "-q"})
		if err != nil {
			t.Fatal(err)
		}
		got := output.String()

		want := fmt.Sprintf("level=info msg=\"Data directory clean (3 directories scanned)\" dataDir=%s\n", dataDir)

		if diff := cmp.Diff(want, got); diff != "" {
			// full output
			fmt.Printf("got = %+v\n", got)

			t.Errorf("Output mismatch (-want +got):\n%s", diff)
		}
	})

	t.Run("Generic files", func(t *testing.T) {
		t.Run("Valid file", func(t *testing.T) {
			err = os.WriteFile(filepath.Join(dataDir, "gitlab-org", "advisories-community", "README.md"), []byte{}, 0600)
			if err != nil {
				t.Fatal(err)
			}
			err = os.WriteFile(filepath.Join(dataDir, "gitlab-org", "advisories-community", ".gitkeep"), []byte{}, 0600)
			if err != nil {
				t.Fatal(err)
			}

			err = os.WriteFile(filepath.Join(dataDir, "gitlab-org", "advisories-community", "ignore"), []byte{}, 0600)
			if err != nil {
				t.Fatal(err)
			}

			err = os.WriteFile(filepath.Join(dataDir, "gitlab-org", "advisories-community", ".DS_Store"), []byte{}, 0600)
			if err != nil {
				t.Fatal(err)
			}

			err = app.Run([]string{os.Args[0], "validate"})
			if err != nil {
				t.Errorf("Unexpected error: %s", err)
			}
		})
		t.Run("Invalid file", func(t *testing.T) {
			unexpectedFile := filepath.Join(dataDir, "gitlab-org", "advisories-community", "result.json")
			err = os.WriteFile(unexpectedFile, []byte("{}"), 0600)
			if err != nil {
				t.Fatal(err)
			}
			defer os.Remove(unexpectedFile)

			err = app.Run([]string{os.Args[0], "validate"})
			if err == nil {
				t.Errorf("Expected error on %q, got: nil", unexpectedFile)
				return
			}
			want := `Unexpected filename "result.json"`
			if err.Error() != want {
				t.Errorf("Expected error to be %q, got: %s", want, err)
			}
		})
	})

	t.Run("Dependencies", func(t *testing.T) {
		deps := filepath.Join(dataDir, "gitlab-org", "declarative-policy", DependenciesFile)
		t.Run("Valid file", func(t *testing.T) {
			b, err := os.ReadFile(filepath.Join("testdata", "projects", "gitlab-org", "declarative-policy", "dependencies.json"))
			if err != nil {
				t.Fatal(err)
			}

			err = os.WriteFile(deps, b, 0600)
			if err != nil {
				t.Fatal(err)
			}

			err = app.Run([]string{os.Args[0], "validate"})
			if err != nil {
				t.Errorf("Unexpected error: %s", err)
			}
		})
		t.Run("Invalid file", func(t *testing.T) {
			err = os.WriteFile(deps, []byte("{broken!}"), 0600)
			if err != nil {
				t.Fatal(err)
			}
			defer os.Remove(deps)

			err = app.Run([]string{os.Args[0], "validate"})
			if err == nil {
				t.Errorf("Expected error on %q got: nil", deps)
				return
			}
			if err.Error() != "invalid character 'b' looking for beginning of object key string" {
				t.Errorf("Expected error on %q, got: %s", deps, err)
			}
		})
	})

	t.Run("Vulnerabilities", func(t *testing.T) {
		vulns := filepath.Join(dataDir, "gitlab-org", "declarative-policy", VulnerabilitiesFile)
		t.Run("Valid file", func(t *testing.T) {
			b, err := os.ReadFile(filepath.Join("testdata", "projects", "gitlab-org", "declarative-policy", "vulnerabilities.json"))
			if err != nil {
				t.Fatal(err)
			}

			err = os.WriteFile(vulns, b, 0600)
			if err != nil {
				t.Fatal(err)
			}

			err = app.Run([]string{os.Args[0], "validate"})
			if err != nil {
				t.Errorf("Unexpected error: %s", err)
			}
		})
		t.Run("Invalid file", func(t *testing.T) {
			err = os.WriteFile(vulns, []byte("{broken!}"), 0600)
			if err != nil {
				t.Fatal(err)
			}
			defer os.Remove(vulns)

			err = app.Run([]string{os.Args[0], "validate"})
			if err == nil {
				t.Errorf("Expected error on %q got: nil", vulns)
				return
			}
			if err.Error() != "error unmarshaling JSON: while decoding JSON: json: cannot unmarshal object into Go value of type []main.Vulnerability" {
				t.Errorf("Expected error on %q, got: %s", vulns, err)
			}
		})
	})

	t.Run("CI configuration", func(t *testing.T) {
		ciConfig := filepath.Join(dataDir, "gitlab-org", "declarative-policy", CIConfigFile)
		t.Run("Valid file", func(t *testing.T) {
			b, err := os.ReadFile(filepath.Join("testdata", "projects", "gitlab-org", "declarative-policy", "ci-config.json"))
			if err != nil {
				t.Fatal(err)
			}

			err = os.WriteFile(ciConfig, b, 0600)
			if err != nil {
				t.Fatal(err)
			}

			err = app.Run([]string{os.Args[0], "validate"})
			if err != nil {
				t.Errorf("Unexpected error: %s", err)
			}
		})
		t.Run("Invalid file", func(t *testing.T) {
			err = os.WriteFile(ciConfig, []byte("{broken!}"), 0600)
			if err != nil {
				t.Fatal(err)
			}
			defer os.Remove(ciConfig)

			err = app.Run([]string{os.Args[0], "validate"})
			if err == nil {
				t.Errorf("Expected error on %q got: nil", ciConfig)
				return
			}
			if err.Error() != "invalid character 'b' looking for beginning of object key string" {
				t.Errorf("Expected error on %q, got: %s", ciConfig, err)
			}
		})
	})

	t.Run("Violations", func(t *testing.T) {
		v := filepath.Join(dataDir, "gitlab-org", "declarative-policy", ViolationsFile)
		t.Run("Valid file", func(t *testing.T) {
			b, err := os.ReadFile(filepath.Join("testdata", "projects", "gitlab-org", "declarative-policy", "violations.json"))
			if err != nil {
				t.Fatal(err)
			}

			err = os.WriteFile(v, b, 0600)
			if err != nil {
				t.Fatal(err)
			}

			err = app.Run([]string{os.Args[0], "validate"})
			if err != nil {
				t.Errorf("Unexpected error: %s", err)
			}
		})
		t.Run("Invalid file", func(t *testing.T) {
			err = os.WriteFile(v, []byte("{broken!}"), 0600)
			if err != nil {
				t.Fatal(err)
			}
			defer os.Remove(v)

			err = app.Run([]string{os.Args[0], "validate"})
			if err == nil {
				t.Errorf("Expected error on %q got: nil", v)
				return
			}
			if err.Error() != "invalid character 'b' looking for beginning of object key string" {
				t.Errorf("Expected error on %q, got: %s", v, err)
			}
		})
	})

	t.Run("Groups", func(t *testing.T) {
		groupFile := filepath.Join(dataDir, "gitlab-org", "5-minute-production-app", GroupMetadataFile)

		err = os.MkdirAll(filepath.Join(dataDir, "gitlab-org", "5-minute-production-app"), 0700)
		if err != nil {
			t.Fatal(err)
		}

		t.Run("Valid file", func(t *testing.T) {
			b, err := os.ReadFile(filepath.Join("testdata", "projects", "gitlab-org", "5-minute-production-app", "group.json"))
			if err != nil {
				t.Fatal(err)
			}

			err = os.WriteFile(groupFile, b, 0600)
			if err != nil {
				t.Fatal(err)
			}

			err = app.Run([]string{os.Args[0], "validate"})
			if err != nil {
				t.Errorf("Unexpected error: %s", err)
			}
		})
		t.Run("Invalid file", func(t *testing.T) {
			err = os.WriteFile(groupFile, []byte("{broken!}"), 0600)
			if err != nil {
				t.Fatal(err)
			}
			defer os.Remove(groupFile)

			err = app.Run([]string{os.Args[0], "validate"})
			if err == nil {
				t.Errorf("Expected error on %q got: nil", groupFile)
				return
			}
			if err.Error() != "invalid character 'b' looking for beginning of object key string" {
				t.Errorf("Expected error on %q, got: %s", groupFile, err)
			}
		})
	})

	t.Run("Properties", func(t *testing.T) {
		propertiesFile := filepath.Join(dataDir, "gitlab-org", "advisories-community", PropertiesFile)
		t.Run("Valid file", func(t *testing.T) {
			propertiesYAML := `
categories:
  - "product"
  - "library"
  - "website"
  - "api"
  - "service"
  - "internal"
  - "external"
  - "green_data"
  - "yellow_data"
  - "orange_data"
  - "red_data"
  - "3rd_party"
  - "demo"
  - "poc"
  - "team"
  - "temporary"
  - "deprecated"
  - "marked_for_deletion"
  - "keep_private"
  - "docs"
  - "tooling"

urls:
  - https://gitlab.com
`

			err = os.WriteFile(propertiesFile, []byte(propertiesYAML), 0600)
			if err != nil {
				t.Fatal(err)
			}

			err = app.Run([]string{os.Args[0], "validate"})
			if err != nil {
				t.Errorf("Unexpected error: %s", err)
			}
		})

		t.Run("Invalid categories", func(t *testing.T) {
			propertiesYAML := `
categories:
  - "invalid category"
`
			err = os.WriteFile(propertiesFile, []byte(propertiesYAML), 0600)
			if err != nil {
				t.Fatal(err)
			}

			err = app.Run([]string{os.Args[0], "validate"})
			if err == nil {
				t.Errorf("Expected error on %q got: nil", propertiesFile)
				return
			}
			if err.Error() != "error unmarshaling JSON: while decoding JSON: unknown category: \"invalid category\"" {
				t.Errorf("Expected error on %q, got: %s", propertiesFile, err)
			}
		})

		t.Run("Invalid URLs", func(t *testing.T) {
			propertiesYAML := `
categories:
  - "website"

urls:
  - "http:/test"
`
			err = os.WriteFile(propertiesFile, []byte(propertiesYAML), 0600)
			if err != nil {
				t.Fatal(err)
			}
			defer os.Remove(propertiesFile)

			err = app.Run([]string{os.Args[0], "validate"})
			if err == nil {
				t.Errorf("Expected error on %q got: nil", propertiesFile)
				return
			}
			if err.Error() != "error unmarshaling JSON: while decoding JSON: invalid url, hostname is empty" {
				t.Errorf("Expected error on %q, got: %s", propertiesFile, err)
			}
		})

		t.Run("Invalid dependency regexp", func(t *testing.T) {
			propertiesYAML := `
categories:
  - "website"

dependencies:
  - name: "test"
    version_extract:
      file_path: Dockerfile
      regexp: '['
`
			err = os.WriteFile(propertiesFile, []byte(propertiesYAML), 0600)
			if err != nil {
				t.Fatal(err)
			}
			defer os.Remove(propertiesFile)

			err = app.Run([]string{os.Args[0], "validate"})
			if err == nil {
				t.Errorf("Expected error on %q got: nil", propertiesFile)
				return
			}
			if err.Error() != "Dependency \"test\": error parsing regexp: missing closing ]: `[`" {
				t.Errorf("Expected error on %q, got: %s", propertiesFile, err)
			}
		})
	})

	t.Run("protected_branches", func(t *testing.T) {
		v := filepath.Join(dataDir, "gitlab-org", "declarative-policy", ProtectedBranchesFile)
		t.Run("Valid file", func(t *testing.T) {
			b, err := os.ReadFile(filepath.Join("testdata", "projects", "gitlab-org", "declarative-policy", "protected_branches.json"))
			if err != nil {
				t.Fatal(err)
			}

			err = os.WriteFile(v, b, 0600)
			if err != nil {
				t.Fatal(err)
			}

			err = app.Run([]string{os.Args[0], "validate"})
			if err != nil {
				t.Errorf("Unexpected error: %s", err)
			}
		})
		t.Run("Invalid file", func(t *testing.T) {
			err = os.WriteFile(v, []byte("{broken!}"), 0600)
			if err != nil {
				t.Fatal(err)
			}
			defer os.Remove(v)

			err = app.Run([]string{os.Args[0], "validate"})
			if err == nil {
				t.Errorf("Expected error on %q got: nil", v)
				return
			}
			if err.Error() != "invalid character 'b' looking for beginning of object key string" {
				t.Errorf("Expected error on %q, got: %s", v, err)
			}
		})
	})
}
