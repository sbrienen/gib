package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"testing"

	"github.com/google/go-cmp/cmp"
	log "github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
)

func TestSyncCmd(t *testing.T) {
	mux, ts := testServer(t)
	defer ts.Close()

	projectsJSON, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "projects.json"))
	if err != nil {
		t.Fatal(err)
	}

	var projects []*gitlab.Project
	err = json.Unmarshal(projectsJSON, &projects)
	if err != nil {
		t.Fatal(err)
	}

	var paginatedProjectsJSON = make([][]byte, 2)
	paginatedProjectsJSON[0], err = json.Marshal(projects[:len(projects)/2])
	if err != nil {
		t.Fatal(err)
	}
	paginatedProjectsJSON[1], err = json.Marshal(projects[len(projects)/2:])
	if err != nil {
		t.Fatal(err)
	}

	// simulate the group projects endpoint
	mux.HandleFunc("/api/v4/groups/gitlab-org/projects", func(w http.ResponseWriter, r *http.Request) {
		q := r.URL.Query()

		if diff := cmp.Diff(q["per_page"], []string{"100"}); diff != "" {
			t.Errorf("per_page mismatch (-want, +got): %s", diff)
		}

		w.Header().Set("x-per-page", "100")
		w.Header().Set("x-total-pages", "2")

		// Simulate pagination
		page := q.Get("page")
		w.Header().Set("x-page", page)
		switch page {
		case "1":
			w.Header().Set("x-next-page", "2")
		case "2":
		default:
			t.Errorf("Unexpected page: %v", page)
		}

		pageInt, err := strconv.Atoi(page)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Fprintln(w, string(paginatedProjectsJSON[pageInt-1]))

	}).Methods(http.MethodGet)

	subgroupsJSON, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "subgroups.json"))
	if err != nil {
		t.Fatal(err)
	}

	var groups []*gitlab.Group
	err = json.Unmarshal(subgroupsJSON, &groups)
	if err != nil {
		t.Fatal(err)
	}

	var paginatedSubgroupsJSON = make([][]byte, 2)
	paginatedSubgroupsJSON[0], err = json.Marshal(groups[:len(groups)/2])
	if err != nil {
		t.Fatal(err)
	}
	paginatedSubgroupsJSON[1], err = json.Marshal(groups[len(groups)/2:])
	if err != nil {
		t.Fatal(err)
	}

	// simulate the group projects endpoint
	mux.HandleFunc("/api/v4/groups/gitlab-org/subgroups", func(w http.ResponseWriter, r *http.Request) {
		q := r.URL.Query()

		if diff := cmp.Diff(q["per_page"], []string{"100"}); diff != "" {
			t.Errorf("per_page mismatch (-want, +got): %s", diff)
		}

		// Simulate pagination
		page := q.Get("page")
		w.Header().Set("x-page", page)
		switch page {
		case "1":
			w.Header().Set("x-next-page", "2")
		case "2":
		default:
			t.Errorf("Unexpected page: %v", page)
		}

		pageInt, err := strconv.Atoi(page)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Fprintln(w, string(paginatedSubgroupsJSON[pageInt-1]))
	}).Methods(http.MethodGet)

	fiveMinAppProjectsJSON, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "5-minute-production-app", "projects.json"))
	if err != nil {
		t.Fatal(err)
	}

	mux.HandleFunc("/api/v4/groups/gitlab-org%2F5-minute-production-app/projects", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, string(fiveMinAppProjectsJSON))
	}).Methods(http.MethodGet)

	// catch urls like
	// /api/v4/groups/gitlab-org%2Farchitecture
	// /api/v4/groups/gitlab-org%2Farchitecture/subgroups
	// /api/v4/groups/gitlab-org%2Fci-cd/projects
	// [...]
	// And return empty arrays (stop walking in the data dir)
	mux.HandleFunc("/api/v4/groups/gitlab-org%2F{subgroup:(?:5-minute-production-app|architecture|build|ci-cd)}/{action:(?:projects|subgroups)}", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "[]")
	}).Methods(http.MethodGet)

	// Create temp data-dir
	dataDir, err := os.MkdirTemp("", "datadir")
	if err != nil {
		log.Fatal(err)
	}
	defer os.RemoveAll(dataDir)

	// Configure dataDir with a main `gitlab-org` group, and an existing project
	err = os.MkdirAll(filepath.Join(dataDir, "gitlab-org", "advisories-community"), 0700)
	if err != nil {
		t.Fatal(err)
	}

	// This file will be updated
	err = os.WriteFile(filepath.Join(dataDir, "gitlab-org", "advisories-community", ProjectMetadataFile), []byte("{}"), 0600)
	if err != nil {
		t.Fatal(err)
	}

	os.Setenv("GITLAB_API_TOKEN", "asdf123")
	os.Setenv("GITLAB_API_BASEURL", ts.URL)
	os.Setenv("DATA_DIR_PATH", dataDir)

	log.SetFormatter(&log.TextFormatter{
		DisableColors:    true,
		DisableTimestamp: true,
	})

	output := new(bytes.Buffer)
	app := NewApp()
	app.Writer = output

	t.Run("Correct output", func(t *testing.T) {
		err = app.Run([]string{os.Args[0], "sync"})
		if err != nil {
			t.Fatal(err)
		}
		got := output.String()

		want :=
			`level=info msg="Fetching group projects" group=gitlab-org
level=info msg="Found 20 project(s)" group=gitlab-org
level=info msg="New project created" name="GitLab OmniAuth OpenID Connect" path=gitlab-org/gitlab-omniauth-openid-connect
level=info msg="Updating project" name="GitLab OmniAuth OpenID Connect" path=gitlab-org/gitlab-omniauth-openid-connect
level=info msg="New project created" name=agg-test1 path=gitlab-org/agg-test1
level=info msg="Updating project" name=agg-test1 path=gitlab-org/agg-test1
level=info msg="New project created" name=samtest path=gitlab-org/samtest
level=info msg="Updating project" name=samtest path=gitlab-org/samtest
level=info msg="Updating project" name=advisories-community path=gitlab-org/advisories-community
level=info msg="New project created" name=file-mirror path=gitlab-org/file-mirror
level=info msg="Updating project" name=file-mirror path=gitlab-org/file-mirror
level=info msg="New project created" name=file path=gitlab-org/file
level=info msg="Updating project" name=file path=gitlab-org/file
level=info msg="New project created" name=ruby-magic path=gitlab-org/ruby-magic
level=info msg="Updating project" name=ruby-magic path=gitlab-org/ruby-magic
level=info msg="New project created" name=declarative-policy path=gitlab-org/declarative-policy
level=info msg="Updating project" name=declarative-policy path=gitlab-org/declarative-policy
level=info msg="New project created" name=mail-smtp_pool path=gitlab-org/mail-smtp_pool
level=info msg="Updating project" name=mail-smtp_pool path=gitlab-org/mail-smtp_pool
level=info msg="New project created" name="Jschafer Test Blank Proj" path=gitlab-org/jschafer-test-blank-proj
level=info msg="Updating project" name="Jschafer Test Blank Proj" path=gitlab-org/jschafer-test-blank-proj
level=info msg="New project created" name="GitLab fork of fog-google" path=gitlab-org/gitlab-fog-google
level=info msg="Updating project" name="GitLab fork of fog-google" path=gitlab-org/gitlab-fog-google
level=info msg="New project created" name="Gitlab Pry Byebug" path=gitlab-org/gitlab-pry-byebug
level=info msg="Updating project" name="Gitlab Pry Byebug" path=gitlab-org/gitlab-pry-byebug
level=info msg="New project created" name="GitLab Codesandbox Client" path=gitlab-org/gitlab-codesandbox-client
level=info msg="Updating project" name="GitLab Codesandbox Client" path=gitlab-org/gitlab-codesandbox-client
level=info msg="New project created" name="GitLab Docs Shell" path=gitlab-org/docs-shell
level=info msg="Updating project" name="GitLab Docs Shell" path=gitlab-org/docs-shell
level=info msg="New project created" name="Standalone Security Hardening" path=gitlab-org/standalone-security-hardening
level=info msg="Updating project" name="Standalone Security Hardening" path=gitlab-org/standalone-security-hardening
level=info msg="New project created" name=pg_query path=gitlab-org/pg_query
level=info msg="Updating project" name=pg_query path=gitlab-org/pg_query
level=info msg="New project created" name=libpg_query path=gitlab-org/libpg_query
level=info msg="Updating project" name=libpg_query path=gitlab-org/libpg_query
level=info msg="New project created" name="Waypoint Images" path=gitlab-org/waypoint-images
level=info msg="Updating project" name="Waypoint Images" path=gitlab-org/waypoint-images
level=info msg="New project created" name="Snowplow Micro Configuration" path=gitlab-org/snowplow-micro-configuration
level=info msg="Updating project" name="Snowplow Micro Configuration" path=gitlab-org/snowplow-micro-configuration
level=info msg="New project created" name="Snowplow Micro" path=gitlab-org/snowplow-micro
level=info msg="Updating project" name="Snowplow Micro" path=gitlab-org/snowplow-micro
level=info msg="Fetching subgroups" group=gitlab-org
level=info msg="Found 4 subgroup(s)" group=gitlab-org
level=info msg="New group created" name="5 minute production app" path=gitlab-org/5-minute-production-app
level=info msg="Updating group" name="5 minute production app" path=gitlab-org/5-minute-production-app
level=info msg="New group created" name=Architecture path=gitlab-org/architecture
level=info msg="Updating group" name=Architecture path=gitlab-org/architecture
level=info msg="New group created" name=Build path=gitlab-org/build
level=info msg="Updating group" name=Build path=gitlab-org/build
level=info msg="New group created" name=CI-CD path=gitlab-org/ci-cd
level=info msg="Updating group" name=CI-CD path=gitlab-org/ci-cd
level=info msg="Fetching group projects" group=gitlab-org/5-minute-production-app
level=info msg="Found 2 project(s)" group=gitlab-org/5-minute-production-app
level=info msg="New project created" name=Hipio path=gitlab-org/5-minute-production-app/hipio
level=info msg="Updating project" name=Hipio path=gitlab-org/5-minute-production-app/hipio
level=info msg="New project created" name="Static Template" path=gitlab-org/5-minute-production-app/static-template
level=info msg="Updating project" name="Static Template" path=gitlab-org/5-minute-production-app/static-template
level=info msg="Fetching subgroups" group=gitlab-org/5-minute-production-app
level=info msg="Found 0 subgroup(s)" group=gitlab-org/5-minute-production-app
level=info msg="Fetching group projects" group=gitlab-org/architecture
level=info msg="Found 0 project(s)" group=gitlab-org/architecture
level=info msg="Fetching subgroups" group=gitlab-org/architecture
level=info msg="Found 0 subgroup(s)" group=gitlab-org/architecture
level=info msg="Fetching group projects" group=gitlab-org/build
level=info msg="Found 0 project(s)" group=gitlab-org/build
level=info msg="Fetching subgroups" group=gitlab-org/build
level=info msg="Found 0 subgroup(s)" group=gitlab-org/build
level=info msg="Fetching group projects" group=gitlab-org/ci-cd
level=info msg="Found 0 project(s)" group=gitlab-org/ci-cd
level=info msg="Fetching subgroups" group=gitlab-org/ci-cd
level=info msg="Found 0 subgroup(s)" group=gitlab-org/ci-cd
`

		if diff := cmp.Diff(want, got); diff != "" {
			// full output
			fmt.Printf("got = %+v\n", got)

			t.Errorf("Output mismatch (-want +got):\n%s", diff)
		}
	})

	t.Run("Project folders and metadata", func(t *testing.T) {
		for _, p := range projects {
			want, err := os.ReadFile(filepath.Join("testdata", "projects", "gitlab-org", p.Path, "project.json"))
			if err != nil {
				t.Fatal(err)
			}

			got, err := os.ReadFile(filepath.Join(dataDir, "gitlab-org", p.Path, "project.json"))
			if err != nil {
				files, err := os.ReadDir(filepath.Join(dataDir, "gitlab-org", p.Path))
				if err != nil {
					log.Fatal(err)
				}

				for _, file := range files {
					fmt.Println(file.Name())
				}
				t.Fatal(err)
			}

			if diff := cmp.Diff(want, got); diff != "" {
				// full output
				fmt.Printf("got = %s\n", got)

				t.Errorf("Output mismatch (-want +got):\n%s", diff)
			}
		}
	})

	t.Run("SubGroups", func(t *testing.T) {
		t.Run("Folders created", func(t *testing.T) {
			for _, g := range groups {
				if _, err := os.Stat(filepath.Join(dataDir, "gitlab-org", g.Path)); os.IsNotExist(err) {
					t.Errorf("Group %q folder was not created", g.Path)
				}
				if _, err := os.Stat(filepath.Join(dataDir, "gitlab-org", g.Path, "group.json")); os.IsNotExist(err) {
					t.Errorf("Group %q metadata was not created", g.Path)
				}
			}
		})
		t.Run("Projects folders created", func(t *testing.T) {
			var fiveMinAppProjects []*gitlab.Project
			err = json.Unmarshal(fiveMinAppProjectsJSON, &fiveMinAppProjects)
			if err != nil {
				t.Fatal(err)
			}
			for _, p := range fiveMinAppProjects {
				if _, err := os.Stat(filepath.Join(dataDir, "gitlab-org", "5-minute-production-app", p.Path)); os.IsNotExist(err) {
					t.Errorf("Project %q folder was not created", p.Path)
				}
				if _, err := os.Stat(filepath.Join(dataDir, "gitlab-org", "5-minute-production-app", p.Path, "project.json")); os.IsNotExist(err) {
					t.Errorf("Project %q metadata was not created", p.Path)
				}
			}
		})
	})

	t.Run("Ignore directories with 'ignore' files", func(t *testing.T) {
		err = os.MkdirAll(filepath.Join(dataDir, "gitlab-org", "security"), 0700)
		if err != nil {
			t.Fatal(err)
		}
		// Ignore the group "gitlab-org/security"
		// Since the http test server doesn't know this route, this test will fail if we try to get the projects of this group
		err = os.WriteFile(filepath.Join(dataDir, "gitlab-org", "security", "ignore"), []byte{}, 0644)
		if err != nil {
			t.Fatal(err)
		}
		err = app.Run([]string{os.Args[0], "sync"})
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Traverse directories with 'ignore' files", func(t *testing.T) {
		// In this test, gitlab-org/ci-cd contains an `ignore` file, but also a subdirectory `shared-runners`.
		// gitlab-org/ci-cd/shared-runners should be synced, but not gitlab-org/ci-cd

		CICDSharedRunnersProjects, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "ci-cd", "shared-runners_projects.json"))
		if err != nil {
			t.Fatal(err)
		}

		mux.HandleFunc("/api/v4/groups/gitlab-org%2Fci-cd%2Fshared-runners/projects", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, string(CICDSharedRunnersProjects))
		}).Methods(http.MethodGet)

		mux.HandleFunc("/api/v4/groups/gitlab-org%2Fci-cd%2Fshared-runners/subgroups", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, "[]")
		}).Methods(http.MethodGet)

		// Ignore the group "gitlab-org/ci-cd"
		err = os.WriteFile(filepath.Join(dataDir, "gitlab-org", "ci-cd", IgnoreFile), []byte{}, 0644)
		if err != nil {
			t.Fatal(err)
		}

		// gitlab-org/ci-cd is ignored, but traversed. Therefore this directory will be synced:
		err = os.MkdirAll(filepath.Join(dataDir, "gitlab-org", "ci-cd", "shared-runners"), 0700)
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(filepath.Join(dataDir, "gitlab-org", "ci-cd", "shared-runners"))

		err = app.Run([]string{os.Args[0], "sync"})
		if err != nil {
			t.Fatal(err)
		}

		t.Run("Projects folders created", func(t *testing.T) {
			var sharedRunnerProjects []*gitlab.Project
			err = json.Unmarshal(CICDSharedRunnersProjects, &sharedRunnerProjects)
			if err != nil {
				t.Fatal(err)
			}

			for _, p := range sharedRunnerProjects {
				if _, err := os.Stat(filepath.Join(dataDir, "gitlab-org", "ci-cd", "shared-runners", p.Path)); os.IsNotExist(err) {
					t.Errorf("Project %q folder was not created", p.Path)
				}
				if _, err := os.Stat(filepath.Join(dataDir, "gitlab-org", "ci-cd", "shared-runners", p.Path, "project.json")); os.IsNotExist(err) {
					t.Errorf("Project %q metadata was not created", p.Path)
				}
			}
		})
	})

	t.Run("Sync standalone projects", func(t *testing.T) {
		// In this test, gitlab-org/ci-cd contains an `ignore` file, but also a subdirectory `docker-machine`, in turn containing a `properties.yml` files.
		// gitlab-org/ci-cd/docker-machine is a "standalone project", and should be synced, but not gitlab-org/ci-cd.
		dmProjectJSON, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "ci-cd", "docker-machine.json"))
		if err != nil {
			t.Fatal(err)
		}

		mux.HandleFunc("/api/v4/projects/gitlab-org%2Fci-cd%2Fdocker-machine", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, string(dmProjectJSON))
		}).Methods(http.MethodGet)

		// Ignore the group "gitlab-org/ci-cd"
		err = os.WriteFile(filepath.Join(dataDir, "gitlab-org", "ci-cd", IgnoreFile), []byte{}, 0644)
		if err != nil {
			t.Fatal(err)
		}

		dmPath := filepath.Join(dataDir, "gitlab-org", "ci-cd", "docker-machine")
		// gitlab-org/ci-cd is ignored, but traversed. Therefore this project will be synced:
		err = os.MkdirAll(dmPath, 0700)
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(dmPath)

		err = os.WriteFile(filepath.Join(dmPath, PropertiesFile), []byte("categories:"), 0644)
		if err != nil {
			t.Fatal(err)
		}

		err = app.Run([]string{os.Args[0], "sync"})
		if err != nil {
			t.Fatal(err)
		}

		t.Run("have projects.json", func(t *testing.T) {
			if _, err := os.Stat(filepath.Join(dmPath, "project.json")); os.IsNotExist(err) {
				t.Error("Project \"docker-machine\" was not synced")
			}
		})
	})

	t.Run("Ignore archived projects", func(t *testing.T) {
		archievedProject, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "archived.json"))
		if err != nil {
			t.Fatal(err)
		}

		mux.HandleFunc("/api/v4/groups/with-archived/projects", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, string(fmt.Sprintf("[%s]", archievedProject)))
		}).Methods(http.MethodGet)

		mux.HandleFunc("/api/v4/groups/with-archived/subgroups", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, "[]")
		}).Methods(http.MethodGet)

		err = os.Mkdir(filepath.Join(dataDir, "with-archived"), 0700)
		if err != nil {
			t.Fatal(err)
		}
		err = app.Run([]string{os.Args[0], "sync"})
		if err != nil {
			t.Fatal(err)
		}

		files, err := os.ReadDir(filepath.Join(dataDir, "with-archived"))
		if err != nil {
			log.Fatal(err)
		}

		for _, f := range files {
			if f.IsDir() && f.Name() == "gitlab-developement-kit" {
				t.Error("Project 'gitlab-developement-kit' is archived and should not be synced")
			}
		}

		t.Run("Clean-up archived projects", func(t *testing.T) {
			err = os.Mkdir(filepath.Join(dataDir, "with-archived", "gitlab-developement-kit"), 0700)
			if err != nil {
				t.Fatal(err)
			}
			err = os.WriteFile(filepath.Join(dataDir, "with-archived", "gitlab-developement-kit", ProjectMetadataFile), []byte("{}"), 0600)
			if err != nil {
				t.Fatal(err)
			}
			err = app.Run([]string{os.Args[0], "sync"})
			if err != nil {
				t.Fatal(err)
			}

			files, err := os.ReadDir(filepath.Join(dataDir, "with-archived"))
			if err != nil {
				log.Fatal(err)
			}

			for _, f := range files {
				if f.IsDir() && f.Name() == "gitlab-developement-kit" {
					t.Error("Project 'gitlab-developement-kit' is archived and should have been removed")
				}
			}
		})
	})

	t.Run("Clean-up projects", func(t *testing.T) {
		err = os.Mkdir(filepath.Join(dataDir, "gitlab-org", "old-project"), 0700)
		if err != nil {
			t.Fatal(err)
		}
		err = os.WriteFile(filepath.Join(dataDir, "gitlab-org", "old-project", ProjectMetadataFile), []byte("{}"), 0600)
		if err != nil {
			t.Fatal(err)
		}
		err = app.Run([]string{os.Args[0], "sync"})
		if err != nil {
			t.Fatal(err)
		}

		files, err := os.ReadDir(filepath.Join(dataDir, "gitlab-org"))
		if err != nil {
			log.Fatal(err)
		}

		for _, f := range files {
			if f.IsDir() && f.Name() == "old-project" {
				t.Error("Project 'old-project' should have been deleted")
			}
		}
	})

	t.Run("Clean-up groups", func(t *testing.T) {
		err = os.Mkdir(filepath.Join(dataDir, "gitlab-org", "old-group"), 0700)
		if err != nil {
			t.Fatal(err)
		}
		err = os.WriteFile(filepath.Join(dataDir, "gitlab-org", "old-group", GroupMetadataFile), []byte("{}"), 0600)
		if err != nil {
			t.Fatal(err)
		}

		err = os.Mkdir(filepath.Join(dataDir, "gitlab-org", "ignored-group"), 0700)
		if err != nil {
			t.Fatal(err)
		}
		err = os.WriteFile(filepath.Join(dataDir, "gitlab-org", "ignored-group", IgnoreFile), []byte("{}"), 0600)
		if err != nil {
			t.Fatal(err)
		}

		err = os.WriteFile(filepath.Join(dataDir, "gitlab-org", "ignored-group", ".DS_Store"), []byte("{}"), 0600)
		if err != nil {
			t.Fatal(err)
		}

		err = app.Run([]string{os.Args[0], "sync"})
		if err != nil {
			t.Fatal(err)
		}

		files, err := os.ReadDir(filepath.Join(dataDir, "gitlab-org"))
		if err != nil {
			log.Fatal(err)
		}

		for _, f := range files {
			if f.IsDir() {
				if f.Name() == "old-group" {
					t.Error("Group 'old-group' should have been deleted")
				}
				if f.Name() == "ignored-group" {
					t.Error("Group 'ignored-group' should have been deleted")
				}
			}
		}
	})

	t.Run("Product projects", func(t *testing.T) {
		propertiesYAML :=
			`categories:
  - product
  - api
  - internal
  - red_data
`

		declarativePolicyPath := filepath.Join(dataDir, "gitlab-org", "declarative-policy")
		PropertiesFile := filepath.Join(declarativePolicyPath, "properties.yml")

		err = os.WriteFile(PropertiesFile, []byte(propertiesYAML), 0600)
		if err != nil {
			t.Fatal(err)
		}

		depsAPI, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "declarative-policy", "dependencies.json"))
		if err != nil {
			t.Fatal(err)
		}

		// simulate project dependencies endpoint
		mux.HandleFunc("/api/v4/projects/gitlab-org%2Fdeclarative-policy/dependencies", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, string(depsAPI))
		}).Methods(http.MethodGet)

		vulnsAPI, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "declarative-policy", "vulnerabilities.json"))
		if err != nil {
			t.Fatal(err)
		}

		// simulate project vulnerabilities endpoint
		mux.HandleFunc("/api/v4/projects/gitlab-org%2Fdeclarative-policy/vulnerabilities", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, string(vulnsAPI))
		}).Methods(http.MethodGet)

		ciConfig, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "declarative-policy", "lint.json"))
		if err != nil {
			t.Fatal(err)
		}
		// simulate project CI-configuration endpoint
		mux.HandleFunc("/api/v4/projects/gitlab-org%2Fdeclarative-policy/ci/lint", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, string(ciConfig))
		}).Methods(http.MethodGet)

		pBranches, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "declarative-policy", "protected_branches.json"))
		if err != nil {
			t.Fatal(err)
		}
		// simulate project protected_branches endpoint
		mux.HandleFunc("/api/v4/projects/gitlab-org%2Fdeclarative-policy/protected_branches", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, string(pBranches))
		}).Methods(http.MethodGet)

		t.Run("should store dependency reports", func(t *testing.T) {

			err = app.Run([]string{os.Args[0], "sync"})
			if err != nil {
				t.Fatal(err)
			}

			want, err := os.ReadFile(filepath.Join("testdata", "projects", "gitlab-org", "declarative-policy", "dependencies.json"))
			if err != nil {
				t.Fatal(err)
			}

			got, err := os.ReadFile(filepath.Join(declarativePolicyPath, "dependencies.json"))
			if err != nil {
				if os.IsNotExist(err) {
					t.Errorf("%q was not created", filepath.Join(declarativePolicyPath, "dependencies.json"))
					return
				}
				t.Fatal(err)
			}

			if diff := cmp.Diff(want, got); diff != "" {
				// full output
				fmt.Printf("got = %s\n", got)

				t.Errorf("Output mismatch (-want +got):\n%s", diff)
			}

		})

		t.Run("should store vulnerability reports", func(t *testing.T) {

			err = app.Run([]string{os.Args[0], "sync"})
			if err != nil {
				t.Fatal(err)
			}

			want, err := os.ReadFile(filepath.Join("testdata", "projects", "gitlab-org", "declarative-policy", "vulnerabilities.json"))
			if err != nil {
				t.Fatal(err)
			}

			got, err := os.ReadFile(filepath.Join(declarativePolicyPath, "vulnerabilities.json"))
			if err != nil {
				if os.IsNotExist(err) {
					t.Errorf("%q was not created", filepath.Join(declarativePolicyPath, "vulnerabilities.json"))
					return
				}
				t.Fatal(err)
			}

			if diff := cmp.Diff(want, got); diff != "" {
				// full output
				fmt.Printf("got = %s\n", got)

				t.Errorf("Output mismatch (-want +got):\n%s", diff)
			}

		})

		t.Run("should store CI-configuration reports", func(t *testing.T) {

			err = app.Run([]string{os.Args[0], "sync"})
			if err != nil {
				t.Fatal(err)
			}

			want, err := os.ReadFile(filepath.Join("testdata", "projects", "gitlab-org", "declarative-policy", "ci-config.json"))
			if err != nil {
				t.Fatal(err)
			}

			got, err := os.ReadFile(filepath.Join(declarativePolicyPath, "ci-config.json"))
			if err != nil {
				if os.IsNotExist(err) {
					t.Errorf("%q was not created", filepath.Join(declarativePolicyPath, "ci-config.json"))
					return
				}
				t.Fatal(err)
			}

			if diff := cmp.Diff(want, got); diff != "" {
				// full output
				fmt.Printf("got = %s\n", got)
				t.Errorf("Output mismatch (-want +got):\n%s", diff)
			}

		})

		t.Run("should store protected branches", func(t *testing.T) {

			err = app.Run([]string{os.Args[0], "sync"})
			if err != nil {
				t.Fatal(err)
			}

			want, err := os.ReadFile(filepath.Join("testdata", "projects", "gitlab-org", "declarative-policy", "protected_branches.json"))
			if err != nil {
				t.Fatal(err)
			}

			got, err := os.ReadFile(filepath.Join(declarativePolicyPath, "protected_branches.json"))
			if err != nil {
				if os.IsNotExist(err) {
					t.Errorf("%q was not created", filepath.Join(declarativePolicyPath, "protected_branches.json"))
					return
				}
				t.Fatal(err)
			}

			if diff := cmp.Diff(want, got); diff != "" {
				// full output
				fmt.Printf("got = %s\n", got)
				t.Errorf("Output mismatch (-want +got):\n%s", diff)
			}

		})
	})

	t.Run("Sync sub directory", func(t *testing.T) {
		// In this test, gitlab-org/gitlab-services contains a subdirectory `design.gitlab.com`, in turn containing a `properties.yml` files.
		// Because we use `sync` with an argument, gitlab-org/gitlab-services should not be synced (will result in 404 otherwise)
		projectJSON, err := os.ReadFile(filepath.Join("testdata", "api", "groups", "gitlab-org", "gitlab-services", "design.gitlab.com.json"))
		if err != nil {
			t.Fatal(err)
		}

		mux.HandleFunc("/api/v4/projects/gitlab-org%2Fgitlab-services%2Fdesign%2Egitlab%2Ecom", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, string(projectJSON))
		}).Methods(http.MethodGet)

		dPath := filepath.Join(dataDir, "gitlab-org", "gitlab-services", "design.gitlab.com")
		err = os.MkdirAll(dPath, 0700)
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(filepath.Join(dataDir, "gitlab-org", "gitlab-services"))

		err = os.WriteFile(filepath.Join(dPath, PropertiesFile), []byte("categories:"), 0644)
		if err != nil {
			t.Fatal(err)
		}

		err = app.Run([]string{os.Args[0], "sync", filepath.Join(dataDir, "./gitlab-org/gitlab-services/design.gitlab.com")})
		if err != nil {
			t.Fatal(err)
		}

		t.Run("have projects.json", func(t *testing.T) {
			if _, err := os.Stat(filepath.Join(dPath, "project.json")); os.IsNotExist(err) {
				t.Error("Project \"design.gitlab.com\" was not synced")
			}
		})

		t.Run("fails if directory is not a subtree of DATADIR", func(t *testing.T) {
			err = app.Run([]string{os.Args[0], "sync", filepath.Join("/tmp", "./gitlab-org/gitlab-services/design.gitlab.com")})
			if err == nil {
				t.Error("sync with arg `/tmp/[...]` should return an error")
			}
		})
	})
}
