package main

import (
	"encoding/json"
	"os"
	"path/filepath"

	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
)

// Group is a wrapper around *gitlab.Group
type Group struct {
	*gitlab.Group
	RunnersToken omit   `json:"runners_token,omitempty"`
	StoragePath  string `json:"-"`
}

func (g Group) logger() *logrus.Entry {
	return log.WithFields(log.Fields{
		"name": g.Name,
		"path": g.FullPath,
	})
}

// Save creates or update all the files of the group g
func (g *Group) Save() error {
	logger := g.logger()

	if _, err := os.Stat(g.StoragePath); os.IsNotExist(err) {
		logger.Info("New group created")
		err := os.Mkdir(g.StoragePath, 0700)
		if err != nil {
			return err
		}
	}

	logger.Info("Updating group")
	return g.storeMetadata()
}

func (g Group) storeMetadata() error {
	// save group metadata
	gjson, err := json.MarshalIndent(g, "", "   ")
	if err != nil {
		return err
	}
	return os.WriteFile(filepath.Join(g.StoragePath, GroupMetadataFile), gjson, 0600)
}
