package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	log "github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
)

func TestComplianceCmd(t *testing.T) {
	t.Run("create-issues", func(t *testing.T) {
		mux, ts := testServer(t)
		defer ts.Close()

		labels := []*gitlab.Label{
			{Name: "bug"},
			{Name: "feature"},
			{Name: "violation::dependency_scanning_is_disabled"},
		}

		var paginatedLabelsJSON = make([][]byte, 2)
		var err error
		paginatedLabelsJSON[0], err = json.Marshal(labels[:len(labels)/2])
		if err != nil {
			t.Fatal(err)
		}
		paginatedLabelsJSON[1], err = json.Marshal(labels[len(labels)/2:])
		if err != nil {
			t.Fatal(err)
		}

		// simulate the project labels endpoints
		mux.HandleFunc("/api/v4/projects/987654/labels", func(w http.ResponseWriter, r *http.Request) {
			q := r.URL.Query()

			if diff := cmp.Diff([]string{"100"}, q["per_page"]); diff != "" {
				t.Errorf("per_page mismatch (-want, +got): %s", diff)
			}

			w.Header().Set("x-per-page", "100")
			w.Header().Set("x-total-pages", "2")

			// Simulate pagination
			page := q.Get("page")
			w.Header().Set("x-page", page)
			switch page {
			case "1":
				w.Header().Set("x-next-page", "2")
			case "2":
			default:
				t.Errorf("Unexpected page: %v", page)
			}

			pageInt, err := strconv.Atoi(page)
			if err != nil {
				t.Fatal(err)
			}
			fmt.Fprintln(w, string(paginatedLabelsJSON[pageInt-1]))

		}).Methods(http.MethodGet)

		createdLabels := []*gitlab.Label{}
		mux.HandleFunc("/api/v4/projects/987654/labels", func(w http.ResponseWriter, r *http.Request) {
			decoder := json.NewDecoder(r.Body)
			var label *gitlab.Label
			err := decoder.Decode(&label)
			if err != nil {
				t.Fatal(err)
			}
			createdLabels = append(createdLabels, label)
			j, err := json.Marshal(label)
			if err != nil {
				t.Fatal(err)
			}

			fmt.Fprintln(w, string(j))
		}).Methods(http.MethodPost)

		issues := []*gitlab.Issue{
			{
				ID:     11,
				IID:    1,
				Title:  `gitlab-org/gitlab-runner: DEPENDENCY_SCANNING_DISABLED is not 'false'`,
				WebURL: ts.URL + "/gitlab-org/gitlab-runner/issues/1",
				Labels: gitlab.Labels{"violation::dependency_scanning_is_disabled", "project_id::278964"},
			},
			{
				ID:    33,
				IID:   3,
				Title: "test issue",
			},
		}

		mux.HandleFunc("/api/v4/projects/987654/issues", func(w http.ResponseWriter, r *http.Request) {
			q := r.URL.Query()

			if q["labels"] != nil {
				t.Errorf("Should fetch issues with no label, got: %s", q["labels"])
			}

			if q["state"] != nil {
				t.Errorf("Should fetch issues with all states, got: %s", q["state"])
			}

			j, err := json.Marshal(issues)
			if err != nil {
				t.Fatal(err)
			}

			fmt.Fprintln(w, string(j))
		}).Methods(http.MethodGet)

		mux.HandleFunc("/api/v4/projects/987654/issues", func(w http.ResponseWriter, r *http.Request) {
			decoder := json.NewDecoder(r.Body)
			var opts gitlab.CreateIssueOptions
			err := decoder.Decode(&opts)
			if err != nil {
				t.Fatal(err)
			}

			title := `gitlab-org/gitlab: Secret Detection is not configured`
			if *opts.Title != title {
				t.Errorf("Title should be %q, got: %q", title, *opts.Title)
			}

			description := fmt.Sprintf("Violation detected in project [gitlab-org/gitlab](%s/gitlab-org/gitlab):\r\n\r\nSecret Detection is not configured", ts.URL)
			if diff := cmp.Diff(description, *opts.Description); diff != "" {
				t.Errorf("Description mismatch (-want, +got): %s", diff)
			}

			split := strings.Split([]string(*opts.Labels)[0], ",")
			labels := gitlab.Labels(split)
			opts.Labels = &labels
			if diff := cmp.Diff(&gitlab.Labels{"violation::secret_detection_not_configured", "project_id::278964"}, opts.Labels); diff != "" {
				t.Errorf("Labels mismatch (-want, +got): %s", diff)
			}

			issue := &gitlab.Issue{
				ID:          22,
				IID:         2,
				Title:       title,
				Description: description,
				Labels:      gitlab.Labels{"violation::secret_detection_not_configured", "project_id::250833"},
			}
			issues = append(issues, issue)

			j, err := json.Marshal(issue)
			if err != nil {
				t.Fatal(err)
			}

			fmt.Fprintln(w, string(j))
		}).Methods(http.MethodPost)

		// Create temp data-dir
		tmpDir, err := os.MkdirTemp("", "tmp")
		if err != nil {
			log.Fatal(err)
		}
		defer os.RemoveAll(tmpDir)

		violations := []*Violation{
			{ProjectID: 250833, PathWithNamespace: "gitlab-org/gitlab-runner", Message: "DEPENDENCY_SCANNING_DISABLED is not 'false'", Description: "DEPENDENCY_SCANNING_DISABLED is set to \"true\"", Key: "dependency_scanning_is_disabled"},
			{ProjectID: 278964, PathWithNamespace: "gitlab-org/gitlab", Message: "Secret Detection is not configured", Description: "Secret Detection is not configured", Key: "secret_detection_not_configured"},
		}

		storeTestViolations(t, tmpDir, violations)

		os.Setenv("GITLAB_API_TOKEN", "asdf123")
		os.Setenv("GITLAB_API_BASEURL", ts.URL)
		os.Setenv("CI_PROJECT_ID", "987654")
		os.Setenv("VIOLATIONS_FILE", filepath.Join(tmpDir, "violations.json"))

		log.SetFormatter(&log.TextFormatter{
			DisableColors:    true,
			DisableTimestamp: true,
		})

		output := new(bytes.Buffer)
		app := NewApp()
		app.Writer = output

		t.Run("Correct output", func(t *testing.T) {
			err = app.Run([]string{os.Args[0], "compliance", "create-issues"})
			if err != nil {
				t.Fatal(err)
			}
			got := output.String()

			want :=
				`level=info msg="1 issue(s) already created"
level=info msg="Create new issue" project=gitlab-org/gitlab violation_key=secret_detection_not_configured
`
			if diff := cmp.Diff(want, got); diff != "" {
				t.Errorf("Output mismatch (-want +got):\n%s", diff)
			}
		})

		t.Run("All labels created", func(t *testing.T) {
			got := createdLabels

			want := []*gitlab.Label{
				// "violation::dependency_scanning_is_disabled" should already be created
				// {Name: "violation::dependency_scanning_is_disabled", Color: "silver"},
				{Name: "project_id::250833", Color: "silver"},
				{Name: "project_id::278964", Color: "silver"},
				{Name: "violation::secret_detection_not_configured", Color: "red"},
				{Name: "resolved", Color: "green"},
				{Name: "ignore", Color: "gray"},
			}

			if diff := cmp.Diff(want, got, cmpopts.SortSlices(func(x, y *gitlab.Label) bool { return x.Name < y.Name })); diff != "" {
				t.Errorf("Created labels mismatch (-want +got):\n%s", diff)
			}
		})

		t.Run("Violation with no issue has state=new", func(t *testing.T) {
			got := loadTestViolations(t, tmpDir)

			v := got[len(got)-1]
			if v.State != "new" {
				t.Errorf("[%s] State should be new, got: %s", v.Title(), v.State)
			}

			v.Issue.Labels = strings.Split(v.Issue.Labels[0], ",")
			if diff := cmp.Diff(issues[len(issues)-1], v.Issue); diff != "" {
				t.Errorf("Violation issue mismatch (-want +got):\n%s", diff)
			}
		})

		t.Run("Resolve issue when violation is gone", func(t *testing.T) {
			issue := &gitlab.Issue{
				ID:     11,
				IID:    1,
				Title:  `gitlab-org/gitlab-runner: DEPENDENCY_SCANNING_DISABLED is not 'false'`,
				WebURL: ts.URL + "/gitlab-org/gitlab-runner/issues/1",
				Labels: gitlab.Labels{"violation::dependency_scanning_is_disabled", "project_id::278964"},
			}

			issues = []*gitlab.Issue{issue}

			mux.HandleFunc("/api/v4/projects/987654/issues/1", func(w http.ResponseWriter, r *http.Request) {
				decoder := json.NewDecoder(r.Body)
				var opts gitlab.UpdateIssueOptions
				err := decoder.Decode(&opts)
				if err != nil {
					t.Fatal(err)
				}

				if diff := cmp.Diff(&gitlab.Labels{"resolved"}, opts.AddLabels); diff != "" {
					t.Errorf("Labels mismatch (-want, +got): %s", diff)
				}

				issue.Labels = append(issue.Labels, "resolved")

				j, err := json.Marshal(issue)
				if err != nil {
					t.Fatal(err)
				}

				fmt.Fprintln(w, string(j))
			}).Methods(http.MethodPut)

			// Load violation test file, but remove one to make the corresponding issue resolved
			storeTestViolations(t, tmpDir, []*Violation{})

			output.Reset()
			err = app.Run([]string{os.Args[0], "compliance", "create-issues"})
			if err != nil {
				t.Fatal(err)
			}
			got := output.String()
			want := fmt.Sprintf(`level=info msg="0 issue(s) already created"
level=info msg="Mark violation issue as resolved" URL="%s/gitlab-org/gitlab-runner/issues/1" project=gitlab-org/gitlab-runner title="gitlab-org/gitlab-runner: DEPENDENCY_SCANNING_DISABLED is not 'false'"
`, ts.URL)

			if diff := cmp.Diff(want, got); diff != "" {
				t.Errorf("Output mismatch (-want +got):\n%s", diff)
			}

			t.Run("resolved violation is exported", func(t *testing.T) {
				got := loadTestViolations(t, tmpDir)

				v := got[0]
				if v.State != "resolved" {
					t.Errorf("[%s] State should be resolved, got: %s", v.Title(), v.State)
				}

				// labels are serialized in a comma-separated string
				// See https://github.com/xanzy/go-gitlab/blob/6b645e887f27bdee7a6aa2d9ee7cf95f467f50da/issues.go#L172
				v.Issue.Labels = strings.Split(v.Issue.Labels[0], ",")
				if diff := cmp.Diff(issue, v.Issue); diff != "" {
					t.Errorf("Created labels mismatch (-want +got):\n%s", diff)
				}
			})
		})

		t.Run("Closed issue is re-opened", func(t *testing.T) {
			issue := &gitlab.Issue{
				ID:     44,
				IID:    4,
				WebURL: ts.URL + "/gitlab-org/gitlab/issues/4",
				Labels: gitlab.Labels{""},
				State:  "closed",
				Title:  "gitlab-org/gitlab: violation to reopen",
			}

			issues = []*gitlab.Issue{issue}
			violations = []*Violation{
				{
					PathWithNamespace: "gitlab-org/gitlab",
					Message:           "violation to reopen",
				},
			}

			storeTestViolations(t, tmpDir, violations)

			mux.HandleFunc("/api/v4/projects/987654/issues/4", func(w http.ResponseWriter, r *http.Request) {
				decoder := json.NewDecoder(r.Body)
				var opts gitlab.UpdateIssueOptions
				err := decoder.Decode(&opts)
				if err != nil {
					t.Fatal(err)
				}

				if diff := cmp.Diff(gitlab.String("reopen"), opts.StateEvent); diff != "" {
					t.Errorf("StateEvent mismatch (-want, +got): %s", diff)
				}

				issue.State = "open"

				j, err := json.Marshal(issue)
				if err != nil {
					t.Fatal(err)
				}

				fmt.Fprintln(w, string(j))
			}).Methods(http.MethodPut)

			output.Reset()
			err = app.Run([]string{os.Args[0], "compliance", "create-issues"})
			if err != nil {
				t.Fatal(err)
			}
			got := output.String()
			want := fmt.Sprintf(`level=info msg="reopeing issue" URL="%s/gitlab-org/gitlab/issues/4" project=gitlab-org/gitlab title="gitlab-org/gitlab: violation to reopen"
level=info msg="1 issue(s) already created"
`, ts.URL)

			if diff := cmp.Diff(want, got); diff != "" {
				t.Errorf("Output mismatch (-want +got):\n%s", diff)
			}

			t.Run("reopen violation is exported", func(t *testing.T) {
				got := loadTestViolations(t, tmpDir)

				v := got[0]
				if v.State != "reopened" {
					t.Errorf("[%s] State should be reopened, got: %s", v.Title(), v.State)
				}

				if diff := cmp.Diff(issue, v.Issue); diff != "" {
					t.Errorf("Created labels mismatch (-want +got):\n%s", diff)
				}
			})
		})
		t.Run("Issue with ignore label", func(t *testing.T) {
			issue := &gitlab.Issue{
				ID:     55,
				IID:    5,
				WebURL: ts.URL + "/gitlab-org/gitlab/issues/5",
				State:  "closed",
				Labels: gitlab.Labels{"ignore"},
				Title:  "gitlab-org/gitlab: violation to ignore",
			}
			issues = []*gitlab.Issue{issue}

			violations = []*Violation{
				{
					PathWithNamespace: "gitlab-org/gitlab",
					Message:           "violation to ignore",
				},
			}

			storeTestViolations(t, tmpDir, violations)

			output.Reset()
			err = app.Run([]string{os.Args[0], "compliance", "create-issues"})
			if err != nil {
				t.Fatal(err)
			}
			got := output.String()
			want := fmt.Sprintf(`level=info msg="ignoring violation" URL="%s/gitlab-org/gitlab/issues/5" project=gitlab-org/gitlab title="gitlab-org/gitlab: violation to ignore"
level=info msg="1 issue(s) already created"
`, ts.URL)

			if diff := cmp.Diff(want, got); diff != "" {
				t.Errorf("Output mismatch (-want +got):\n%s", diff)
			}

			t.Run("ignored violation is exported", func(t *testing.T) {
				got := loadTestViolations(t, tmpDir)

				v := got[0]
				if v.State != "ignored" {
					t.Errorf("[%s] State should be ignored, got: %s", v.Title(), v.State)
				}

				if diff := cmp.Diff(issue, v.Issue); diff != "" {
					t.Errorf("Created labels mismatch (-want +got):\n%s", diff)
				}
			})
		})

		t.Run("Resolved issue is unresolved", func(t *testing.T) {
			issue := &gitlab.Issue{
				ID:     66,
				IID:    6,
				WebURL: ts.URL + "/gitlab-org/gitlab/issues/6",
				Labels: gitlab.Labels{labelResolved},
				State:  "open",
				Title:  "gitlab-org/gitlab: violation to unresolve",
			}

			issues = []*gitlab.Issue{issue}
			violations = []*Violation{
				{
					PathWithNamespace: "gitlab-org/gitlab",
					Message:           "violation to unresolve",
				},
			}

			storeTestViolations(t, tmpDir, violations)

			mux.HandleFunc("/api/v4/projects/987654/issues/6", func(w http.ResponseWriter, r *http.Request) {
				decoder := json.NewDecoder(r.Body)
				var opts gitlab.UpdateIssueOptions
				err := decoder.Decode(&opts)
				if err != nil {
					t.Fatal(err)
				}

				if opts.RemoveLabels != nil {
					if diff := cmp.Diff(&gitlab.Labels{labelResolved}, opts.RemoveLabels); diff != "" {
						t.Errorf("RemoveLabels mismatch (-want, +got): %s", diff)
					}

					labels := gitlab.Labels{}
					for _, label := range issue.Labels {
						if label != labelResolved {
							labels = append(labels, label)
						}
					}
					issue.Labels = labels
				}

				if opts.StateEvent != nil {
					if diff := cmp.Diff(gitlab.String("reopen"), opts.StateEvent); diff != "" {
						t.Errorf("StateEvent mismatch (-want, +got): %s", diff)
					}
					issue.State = "open"
				}

				j, err := json.Marshal(issue)
				if err != nil {
					t.Fatal(err)
				}

				fmt.Fprintln(w, string(j))
			}).Methods(http.MethodPut)

			output.Reset()
			err = app.Run([]string{os.Args[0], "compliance", "create-issues"})
			if err != nil {
				t.Fatal(err)
			}
			got := output.String()
			want := fmt.Sprintf(`level=info msg="Remove 'Resolved' label from issue" URL="%s/gitlab-org/gitlab/issues/6" project=gitlab-org/gitlab title="gitlab-org/gitlab: violation to unresolve"
level=info msg="1 issue(s) already created"
`, ts.URL)

			if diff := cmp.Diff(want, got); diff != "" {
				t.Errorf("Output mismatch (-want +got):\n%s", diff)
			}

			if hasResolvedLabel(issue) {
				t.Error("Issue is still resolved")
			}

			t.Run("Close and Resolved issue is unresolved and reopen", func(t *testing.T) {
				issue.State = "closed"
				issue.Labels = gitlab.Labels{labelResolved}

				err = app.Run([]string{os.Args[0], "compliance", "create-issues"})
				if err != nil {
					t.Fatal(err)
				}
				if hasResolvedLabel(issue) {
					t.Error("Issue is still resolved")
				}
				if issue.State != "open" {
					t.Errorf("Issue is not reopen, got issue.State=%s", issue.State)
				}
			})

		})
	})
}

func loadTestViolations(t *testing.T, tmpDir string) []*Violation {
	b, err := os.ReadFile(filepath.Join(tmpDir, "violations.json"))
	if err != nil {
		t.Fatalf("Error loading violations: %v", err)
	}

	var violations []*Violation
	err = json.Unmarshal(b, &violations)
	if err != nil {
		t.Fatalf("Error loading violations: %v", err)
	}
	return violations
}

func storeTestViolations(t *testing.T, tmpDir string, violations []*Violation) {
	violationsData, err := json.Marshal(violations)
	if err != nil {
		t.Fatalf("Error storing violations: %v", err)
	}

	err = os.WriteFile(filepath.Join(tmpDir, "violations.json"), violationsData, 0644)
	if err != nil {
		t.Fatalf("Error storing violations: %v", err)
	}
}
